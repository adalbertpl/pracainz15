from __future__ import division

import cmd
import copy
import os
import sys

from myprogressbar import TerminalProgressBar

if os.environ.get('USE_MY_UTILS_DEV_VERSION')=="1":
    sys.path.append(os.path.expanduser('~/AGH/Python/utils'))
    #print "test"
    pass
elif os.environ.get('USE_MY_UTILS_DEV_VERSION')=="2":
    print "for this value of USE_MY_UTILS_DEV_VERSION program exit"
    raise Exception("Wrong USE_MY_UTILS_DEV_VERSION")
else:
    sys.path.append(os.path.expanduser('./utils-copy'))
    sys.path.append(os.path.expanduser('./utils'))
    #sys.path.append(os.path.expanduser('./utils-local'))
    pass


import time
import numpy as np

from meetfinder import MeetFinderFactory, GridMeetFinder
from particles import BrownianMotion, FlatSimParams

from statutils import *

from mainutils import *
from geomutils import *





class ParticlesCases:
    """ Cases to measurement from Particles Program.

    """

    @staticmethod
    def rMeetVsProbabilityPlot():

        #defaultparams = [1, 1000, [0.5, 0.5, 0.5], 10, 100, 100, 2]
        #rmeetParam = [100, 150, 200, 250, 300, 400, 600, 800, 1000, 1200]
        #particlesNbSeries = [2, 5, 10]
        #repeat = 10000

        params = [1, 1000, 0.5, 0.5, 0.5, 10, 100, 100, 2]
        rmeetParam = range(10, 200, 10)
        particlesNbSeries = [2, 3]
        repeat = 1000
        #TODO: maybe auto adapt number of repeats to probability?

        #testedCode = BrownianMotionMeasure.funrMeetNumberOfParticles
        testedCode = BrownianMotionFunctions.funSelectedSimulationParametersCreator("rMeet", "particlesNumber", FlatSimParams(*params))

        ResultStatUtils.measureResultOfCode(testedCode,
                rmeetParam, particlesNbSeries,
                aggregateResults=BrownianMotionMeasure.aggregateResults, repeat=repeat, progressbar=TerminalProgressBar(1))


    @staticmethod
    def particlesNumberTimePlot(params=None):
        """ Plot time of simulation to particles number.
            Measure performance of BrownianMotion.
            :params: list of parameters for BrownianMotion init (in init format)
        """
        #TODO: progressbar
        #TODO: tuple to list conversion
        #TODO: maybe input should be object with named parameters?
        # if params==None:
        #     #Last parameter (particles number) is changed in next lines

        repeat = 10
        #TODO: stop test when time to long
        testedParticlesNb = [2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15, 20, 25, 30, 35, 40, 45, 50]
        #testedParticlesNb = [20, 50, 100]
        #testedParticlesNb = [2, 3, 5, 7, 10, 15, 20, 30, 40, 50, 60, 80, 100, 125, 150]
        #testedParticlesNb = [100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
        seriesParamValues = ["bruteforce", "octree", "grid", "slowergrid"]

        #Maybe reduce repeat times for long time calculation?
        #Maybe stop calculation one series when time exceeds some limit?
        #Maybe return part of results and plot part of results when calculation is running?
        #Maybe reduce number of steps of simulation when time is too long?

        #def createBrownianMotion(testedParam, seriesValue="octree"):

        testedCode = BrownianMotionFunctions.funParticlesNumberAndMeetFinder

        TimeStatUtils.measureTimeOfCode(testedCode, testedParticlesNb, seriesParamValues, repeat=repeat)

        #matrix of results

def brownianMotionParamsListToDict(extendedParamsList):
    """ Convert extended parameters (simulation parameters, meetfinder etc.) from list to dictionary.
    """
    paramsDict = FlatSimParams(*extendedParamsList[0:9])._asdict()
    #paramsDict.update({"meetFinder": "grid"})
    paramsDict.update({"meetFinder": extendedParamsList[9]})
    return paramsDict

def funLambdaParametersCreator(paramsFun, fun):
    """
    This function should hame small overhead because will be run multiple times?
    :param paramFun: function(testedParam, seriesParam) that will return parameters as dictionary
    :param fun: function that accepts dictionary as only argument;
        function may have default values for keys that are not present when user will want to
        give only part of keys in params
    :return: callable object that will have functions as BrownianMotionMeasure?
    """

    def createdFunction(testedParam, seriesParam):
        return fun(paramsFun(testedParam, seriesParam))

    return createdFunction

def funSelectedParametersCreator(testedParamName, seriesParamName, params, fun):
    """
    This function should hame small overhead because will be run multiple times?
    :param TestedParamName: name of parameter that will be x on chart
    :param seriesParamName: name of parameter that will be series on chart
    :param params: dictionary of arguments of function fun
    :param fun: function that accepts dictionary as only argument;
        function may have default values for keys that are not present when user will want to
        give only part of keys in params
    :return: callable object that will have functions as BrownianMotionMeasure?
    """

    def createdFunction(testedParam, seriesParam):
        params[testedParamName] = testedParam
        params[seriesParamName] = seriesParam

        f = fun(params)
        f.testedParamName = testedParamName
        f.seriesParamName = seriesParamName
        return f

    return createdFunction


class BrownianMotionFunctions(object):
    """ Class containing function that will be run in ParticlesCases.
        Class changes code interface.
    """

    @staticmethod
    def brownianMotionFun(paramDict):
        """

        :param paramDict: dictionary - may contain parameters of
        :return: Object that will run simulation after call, object contains some data
        """

        defaultDict = {"stepTime": 1, "simTime": 100, "rangeX": 1, "rangeY": 1, "rangeZ": 1,
                       "minSpeed": 10, "maxSpeed": 100, "rMeet": 100, "particlesNumber": 2, "meetFinder":"octree",
                    }

        paramDict2 = copy.copy(defaultDict)
        paramDict2.update(paramDict)

        #TODO: meetFinder configuration

        meetFinder = MeetFinderFactory().createMeetFinder(paramDict2["meetFinder"] ,
                        {"rMeet":paramDict2["rMeet"], "position": [0, 0, 0],
                         "size": [paramDict2["rangeX"], paramDict2["rangeY"], paramDict2["rangeZ"]]})
        bm = BrownianMotion(paramDict2["stepTime"], paramDict2["simTime"], [paramDict2["rangeX"],
                            paramDict2["rangeY"], paramDict2["rangeZ"]],
                            paramDict2["minSpeed"], paramDict2["maxSpeed"], paramDict2["rMeet"],paramDict2["particlesNumber"],
                            progressbar=False, meetFinder=meetFinder)
        return BrownianMotionMeasure(bm, params=paramDict2)

    @staticmethod
    def funSelectedSimulationParametersCreator(simTestedParamName, simSeriesParamName, params):
        """ Partially define function parameters. Other two parameters (simTestedParamName, simSeriesParamName) will be used for statistics.

        :param simTestedParamName: name of parameter that will be x on chart
        :param simSeriesParamName: name of parameter that will be series on chart
        :param params: parameters of simulation, that will be used (as FlatSimParams)
        :return: function that will have 2 parameters
        """
        #TODO: function that will use dictionary as parameters

        def createdFunction(testedParam, seriesParam):
            """
            :param testedParam: value of parameter that will be x on chart
            :param seriesParam: value of parameter that will be series on chart
            :return: object that will return result if called and that have calcResultMetric method
            """
            paramsDict = params._asdict()

            #What with repeat parameter - is it needed here?
            paramsDict[simTestedParamName] = testedParam
            paramsDict[simSeriesParamName] = seriesParam

            changedParams = FlatSimParams(**paramsDict)
            #print changedParams

            meetFinder = GridMeetFinder(params.rMeet)
            bm = BrownianMotion(changedParams.stepTime, changedParams.simTime, [changedParams.rangeX, changedParams.rangeY, changedParams.rangeZ],
                                *(changedParams[5:]),
                                progressbar=False, meetFinder=meetFinder)
            return BrownianMotionMeasure(bm)

        #TODO: implement
        return createdFunction


    @staticmethod
    def funParticlesNumberAndMeetFinder(testedParam, seriesValue="octree", repeat=1, defaultparams=None):
        """ Create simulation instance.
        :param testedParam: value of parameter that will be plot
        :param seriesValue: value of parameter that will be shown as series
        """
        if defaultparams==None:
            defaultparams = BrownianMotionMeasure.defaultparams

        params = copy.copy(defaultparams)
        simulationParams = params #Get parameters from outer scope (as closure)
        #TODO: will list from outer scope be accessible from every place?
        #Will it not be garbage collected?

        params[6] = testedParam #Change particles number
        meetFinderName = seriesValue
        initConf = {"position": [0, 0, 0], "size": params[2], "rMeet": params[5]}
        meetFinder = MeetFinderFactory().createMeetFinder(seriesValue, initConf)

        bm = BrownianMotion(*params, progressbar=False, meetFinder=meetFinder)
        return BrownianMotionMeasure(bm)

    @staticmethod
    def funrMeetNumberOfParticles(testedParam, seriesValue=2, repeat=1, defaultparams=None):
        if defaultparams==None:
            defaultparams = BrownianMotionMeasure.defaultparams

        params = copy.copy(defaultparams)
        params[5] = testedParam
        params[6] = seriesValue
        bm = BrownianMotion(*params, progressbar=False)
        return BrownianMotionMeasure(bm)


def runListOfBrownianSimMeasurements():
    #oneMeasurementBrownianSimAdapter([1, 100, 2, 2, 2, 10, 100, 100, 3, "grid"], "particlesNumber", range(2, 22, 2), "rMeet", [50, 75, 100], 10)

    #oneMeasurementBrownianSimAdapter([1, 800, 4, 4, 4, 10, 100, 100, 3, "grid"], "particlesNumber", range(2, 22, 2), "rMeet", [50, 75, 100], 10)

    #oneMeasurementBrownianSimAdapter([1, 100, 2, 2, 2, 10, 100, 100, 2, "grid"], "minSpeed", range(10, 100, 10), "maxSpeed", [100, 150, 200], 10)

    #oneMeasurementBrownianSimAdapter([1, 100, 2, 2, 2, 10, 100, 100, 2, "grid"], "simTime", range(1000, 5000, 500), "particlesNumber", [2, 3, 4], 10)

    #oneMeasurementBrownianSimAdapter([1, 100, 32, 0.5, 0.5, 10, 100, 100, 2, "grid"], "simTime", range(1000, 5000, 500), "particlesNumber", [2, 3, 4], 10)

    #oneMeasurementBrownianSimAdapter([1, 100, 4, 4, 0.5, 10, 100, 100, 2, "grid"], "simTime", range(1000, 5000, 500), "particlesNumber", [2, 3, 4], 10)

    #oneMeasurementBrownianSimAdapter([1, 100, 4, 4, 4, 10, 100, 100, 10, "grid"], "particlesNumber", range(10, 100, 10), "meetFinder", ["grid", "octree"], 10)

    #1 - rMeet
    #oneMeasurementBrownianSimAdapter([1, 3600, 3, 3, 3, 10, 100, 100, 2, "grid"], "rMeet", range(300, 750, 50), "particlesNumber", [2], 1000)

    #test - meetFinder
    oneMeasurementBrownianSimAdapter([1, 100, 2, 2, 2, 10, 100, 100, 3, "grid"], "particlesNumber", range(2, 22, 2), "meetFinder", ["grid", "octree", "bruteforce"], 100)


    #lambdaParams = lambda t,s: \
    #    brownianMotionParamsListToDict([1, 100, t**(1/3), t**(1/3), t**(1/3), 10, 100, 100, int(2*t*s), "grid"])
    #t - volume and number of particles(same density), s - particles density

    #oneMeasurementBrownianSimAdapterLambdaParams(lambdaParams, range(20, 40, 4), [0.25], 100
    #     ,name = "lambda t,s: \
    #       brownianMotionParamsListToDict([1, 100, t**(1/3), t**(1/3), t**(1/3), 10, 100, 100, int(2*t*s), \"grid\"])")

    #random walk to wiener process
    #lambdaParams = lambda t,s: \
    #    brownianMotionParamsListToDict([1, int(300*t*s), 1, 1, 1, 10/(t**(1/3)), 100/(t**(1/3)), 100, 2, "grid"])
    #TODO: problem if parameters will change but name will not change


    #oneMeasurementBrownianSimAdapterLambdaParams(lambdaParams, [16, 20, 24, 28, 32], [1, 2], 20
    #     ,name = "    lambdaParams = lambda t,s: \
    #    brownianMotionParamsListToDict([1, int(300*t*s), 1, 1, 1, 10/(t**(1/3)), 100/(t**(1/3)), 100, 2, \"grid\"])")


    #flattening of cuboid
    #lambdaParams = lambda t,s: \
    #    brownianMotionParamsListToDict([1, 200, t*s, t*s, 8*s/(t*t), 10, 100, 100, 50, "grid"])

    #oneMeasurementBrownianSimAdapterLambdaParams(lambdaParams, [12, 13, 14], [4], 100
    #    ,name = "    lambdaParams = lambda t,s: \
    #        brownianMotionParamsListToDict([1, 1000, t*s, t*s, 8*s/(t*t), 10, 100, 100, 50, \"grid\"])")

def oneMeasurementBrownianSimAdapter(params, testedParamName, testedParamValues, testedSeriesName, testedSeriesValues, repeat):
    """ Function run one result measurement and save results to file

    :param params: list of parameters of function (simulation parameters and meetfinder name)
    :param testedParamName: name of tested parameter
    :param testedParamValues: list of tested parameter values
    :param testedSeriesName: name of series parameter
    :param testedSeriesValues: list of series parameter values
    :param repeat: number of repetition
    """
    paramsDict = FlatSimParams(*params[0:9])._asdict()
    #paramsDict.update({"meetFinder": "grid"})
    paramsDict.update({"meetFinder": params[9]})
    testedCode = funSelectedParametersCreator(testedParamName, testedSeriesName, paramsDict, BrownianMotionFunctions.brownianMotionFun)
    ResultStatUtils.measureResultOfCode(testedCode,
        testedParamValues, testedSeriesValues,
        aggregateResults=BrownianMotionMeasure.aggregateResults, repeat=repeat, progressbar=TerminalProgressBar(1), saveToFile=True, plot=False)


def oneMeasurementBrownianSimAdapterLambdaParams(paramsFun, testedParamValues, testedSeriesValues, repeat, name=None):
    #TODO: maybe refactor
    #paramsDict = FlatSimParams(*params[0:9])._asdict()
    #paramsDict.update({"meetFinder": params[9]})

    testedCode = funLambdaParametersCreator(paramsFun, BrownianMotionFunctions.brownianMotionFun)

    ResultStatUtils.measureResultOfCode(testedCode,
        testedParamValues, testedSeriesValues,
    aggregateResults=BrownianMotionMeasure.aggregateResults, repeat=repeat, progressbar=TerminalProgressBar(1), saveToFile=True, plot=False, name=name)


class CodeMeasure(object):
    #defaultparams? - attribute
    #self.params - should contain default parameters of function (maybe name it default function parameters?)
    #   (testedParam and seriesParam can have any values)

    def __call__(self):
        #Run measured code
        raise Exception("Not implemented")

    def calcResultMetric(self, result):
        raise Exception("Not implemented")

    @staticmethod
    def aggregateResults(repeatedResults):
        raise Exception("Not implemented")


class BrownianMotionMeasure(CodeMeasure):

    #This is used for example in time measure
    defaultparams = [1, 10, [5, 5, 5], 10, 100, 100, 2]
    #defaultparams = [1, 50, [20, 20, 20], 10, 100, 100, 2]


    def __init__(self, bm, params=None, testedParamName=None, testedSeriesName=None):
        """

        :param bm:
        :param params:
        :return:
        """
        #testedParamName - None or ""?
        self.bm = bm
        self.params = params
        self.testedParamName = testedParamName
        self.testedSeriesName = testedSeriesName
        #Params - some structure with data of parameters. Should have __str__ method.

        #if defaultparams==None:
        #    defaultparams = BrownianMotionMeasure.defaultparams

    def __call__(self):
        #Run initialized simulation
        #This call may be measured for time so it should be as short as possible
        # (for measurement of time and measurement of result)
        result = self.bm.simulate()
        #Steps needed as we want divide time by number of steps
        simulationTime = result["result"]  #self.bm.simulationSteps

        if simulationTime=="False":
            self.steps = result["params"][1] / result["params"][0]
        else:
            self.steps = simulationTime / result["params"][0]

        return result

    def calcResultMetric(self, result):
        """ Calculate some metric from result of function run (call) and state of object after function."""
        #Return estimated probability of meetinf with actual parameters
        #return self.bm.meetCount / (self.bm.meetCount + self.bm.notMeetCount)
        #Convert all numbers to True
        result = result["result"]
        if result=="False":
            return False
        else:
            return True

    @staticmethod
    def aggregateResults(repeatedResults):
        #e.g. calculate some estimator
        trueCount = sum(1 for r in repeatedResults if r==True)
        #Probability of particles meet before specified time
        trueProbability = trueCount/len(repeatedResults)
        print "trueProbability", trueProbability
        return trueProbability



class StatCmd(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)

    def do_plotPerformanceComparision(self, line):
        try:
            ParticlesCases.particlesNumberTimePlot()
        except Exception as e:
            print(e)
            print(str(traceback.format_exc()))

    def do_plotProbabilityrMeet(self, line):
        try:
            print "Attention! Not work as expected now - wrong parameters."
            ParticlesCases.rMeetVsProbabilityPlot()
        except Exception as e:
            print(e)
            print(str(traceback.format_exc()))

    def do_runListOfBrownianSimMeasurements(self, line):
        runListOfBrownianSimMeasurements()

    def do_plotTestSelectedParameters(self, line):
        try:
            #Get input from user (what should be tested)
            #Simulation parameters and other special "parameters"
            paramTypes = [float, float, float, float, float, float, float, float, float, str]
            params = [paramType(param) for param, paramType in zip(line.split(), paramTypes)]
            #params = map(float, FlatSimParams(*line.split()))
            testedParamName = raw_input("Tested parameter name:")
            start, end, step = map(int, raw_input("Tested parameter range (start, afterEnd, step):").split())
            testedParamRange = range(start, end, step)
            testedSeriesName = raw_input("Tested series name:")
            start, end, step = map(int, raw_input("Tested series range (start, afterEnd, step):").split())
            testedSeriesRange = range(start, end, step)
            repeat = int(raw_input("Number of repetition:"))

            #Prepare object for computing statistics and compute these statistics
            #testedCode = BrownianMotionMeasure.funSelectedSimulationParametersCreator(testedParamName, testedSeriesName, FlatSimParams(*params))
            paramsDict = FlatSimParams(*params[0:9])._asdict()
            #paramsDict.update({"meetFinder": "grid"})
            paramsDict.update({"meetFinder": params[9]})
            testedCode = funSelectedParametersCreator(testedParamName, testedSeriesName, paramsDict, BrownianMotionFunctions.brownianMotionFun)
            ResultStatUtils.measureResultOfCode(testedCode,
                testedParamRange, testedSeriesRange,
                aggregateResults=BrownianMotionMeasure.aggregateResults, repeat=repeat, progressbar=TerminalProgressBar(1), saveToFile=True)

        except Exception as e:
            print(e)
            print(str(traceback.format_exc()))

if __name__ == "__main__":

    cmd1 = StatCmd()
    #main program loop
    cmd1.cmdloop()

    #import doctest
    #doctest.testmod()
