import unittest
import numpy as np

#Run test from main project directory
from particles import *


class BrownianMotionTest(unittest.TestCase):
    def setUp(self):
        self.bm = BrownianMotion(1, 3000, [5, 5, 5], 10, 100, 100, progressbar=False)

    def test_findMeeting(self):
        particles = map(np.array, [[1, 2], [1, 3], [2, 1], [2, 2]])
        self.assertEqual(self.bm.findMeeting(particles), False)

        particles = map(np.array, [[1, 2], [1, 3], [2, 1], [1, 3.02], [2, 2]])
        self.assertEqual(self.bm.findMeeting(particles), True)

    def test_meets(self):
        particles = map(np.array, [[1, 2], [1, 3]])
        self.assertEqual(
                self.bm.meets(*particles), False)

        particles = map(np.array, [[1, 2], [1, 2.02]])
        self.assertEqual(self.bm.meets(*particles), True)
