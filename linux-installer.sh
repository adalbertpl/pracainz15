#!/bin/sh
#This is linux installer of dependencies of ParticlesCmd and ParticlesGUI programs
#
#Requirements:
# Python 2.7 must be installed. Pip must be installed.
# Numpy and Matplotlib must be installed.
#
#Usage:
#./linux-installer.sh

pip install argparse
pip install progressbar
pip install pycontracts
#pip install plyfile - plyfile has numpy as dependency
