from utils import *


class AlgoMeasure:
    """ Class for measuring algorithms and implementations.
        For comparing times and mistakes.
        Visualization should be in another class.
        >>> algo = AlgoMeasure([pi], exactFun=cos, compareFun=lambda x,y: x==y)
        >>> algo.computeResults()
        >>> algo.results[0] == -1
        True
        >>> sys.stdout.write('skip ');algo.compareResults("name2", [-1.0]) #doctest:+ELLIPSIS
        skip ...
        >>> algo.goodResult(-1 ,0)
        True
        >>> algo.goodResult(2.5, 0)
        False
    """

    def __init__(self, dataCases, exactFun=None, compareFun=None, conf=None):
        """ Initialize object.
            :param dataCases: list of dataCases;
                dataset may be e.g. list of points or one number (e.g. 2.5);
                dataset is input for function
        """
        self.dataCases = dataCases
        self.conf = conf
        self.exactFun = exactFun
        self.compareFun = compareFun

    def goodResult(self, result, i):
        """ Return True if result are good enough.
        """
        #TODO: number?
        return (self.compareFun(result, self.results[i]) == True)
    #__del__():
        #clearGogui();#clear gogui data for later use

    #def printVisualization(self, filename):
        #TODO: where should be this function
        #FIXME: one JSON?
        #string json = gogui::getJSON();
        #TODO: maybe fork gogui or make pull request - not needes, found new version
        #TODO: dalej brakuje zerowania biblioteki, gdy chcę zrobić kolejną symulację
        #przez to potem skala jest zla

        #std::fstream myfile(filename, std::ios_base::out);
        #myfile << json;
    #    pass

    #def clearGogui(self):
    #    clearPoints()
        #gogui::clearSnapshots() #it seems to work

    #def clearPoints(self):
        #cloud.clear()
    #    pass

    def computeResults(self, exactFun=None):
        """ Compute expected exact result for all dataCases. Use given exact function.
            Exact results are needed for comparing with some not exact algorithms and implementations.
            :param exactFun: function returning exact results of exercise
        """
        if exactFun==None:
            exactFun=self.exactFun
        #exact results for given dataset
        self.results = []

        for dataCase in self.dataCases:
            self.results.append(exactFun(dataCase))

        #TODO: visualize data
        #gogui::snapshot();


    def compareResults(self, funName, comparedResults):
        """ Compare result of non exact implementation with exact implementation.
        """

        print ("Accuracy of function: ", funName)

        #TODO: what if algorithm is not deterministic?
        #what if it can be multiple good results?
 
        mistakes = [] #Mistakes of compared algorithm
        #wrongResultsCount = 0
        #FIXME:zero
        #FIXME: points don't show
        #auto activePoints = vector<gogui::ActivePoint>();

        #TODO: maybe show additionally wrong points with all points on one chart
        #clearPoints();

        #for (data in dataset):
        for i in range(0, len(self.dataCases)):

            if not self.goodResult(comparedResults[i], i):
                    #maybe remove count?

                    #FIXME: 
                    #orient: 
                    #case - point
                    #mistake is one case - point,
                    # algorithm has no visualization
                    # visualization of multiple cases
                    #convex hull: 
                    #case - set of points
                    #mistake is result of one case (or one case?) - set of points
                    # algorithm need visualization
                    # visualization of one case
                    # need line segments on visualization
                    #algorytm zamiatania: 
                    #case - set of line segments
                    #mistake - not case - one point ?
                    #algorithm need visualization
                    #result - set of points
                    #need line for visualization?
                    #
                    #Compare results (subclasses?)
                    # good or bad
                    # find wrong elements

                    dataCase = self.dataCases[i]

                    mistake = (dataCase, comparedResults[i])

                    mistakes.append(mistake)

                    #TODO: visualizeMistake(mistake)
                    #visualization - wrong point in another colour
                    #gogui::Point p = gogui::Point(pointRaw[0], pointRaw[1]);
                    #p.setColor("blue");
                    #cloud.push_back(p);

                    #print ("Mistake", mistake)

                    #cout << "Wrong result: \n";
                    #cout << "Point: " << point << "\n";
                    #cout << "Wrong result: " << comparedResults[i] << " , exact result: " << exactResults[i] << "\n\n";*/


        print ("Good results count: ", len(self.dataCases) - len(mistakes))
        print ("Wrong results count: ", len(mistakes))

        #show only wrong points
        #gogui::snapshot(); 

    def measure(self, algoFun, funName=None, count=1):
        """ Measure time of algoFun and get results.
        """

        if funName==None:
            funName == algoFun.__name__

        results = []
        times = []

        for data in self.dataCases:
            #TODO: good time measure; maybe Cython?
            #TODO: what with conf
            #wrapper, conf=self.conf ?
            #TODO: multiple dataCases x multiple runs - which first?
            if count==1:
                start = time.clock()

                result = algoFun(data)

                end = time.clock()
                elapsedTime = end-start; #what unit?
                results.append(result)
                times.append(elapsedTime)            

                #TODO: print time measure

            else:
                #TODO: average of 100 x 100?
                #Multiple counts - we need statistics
                caseTimes = []
                for i in range(count):
                    start = time.clock()

                    result = algoFun(data)

                    end = time.clock()
                    elapsedTime = end-start; #what unit?

                    caseTimes.append(elapsedTime)

                    #TODO: results

                caseAvg = np.mean(caseTimes)
                caseMin = min(caseTimes)
                caseMax = max(caseTimes)
                caseSum = sum(caseTimes)
                #TODO: odchylenie stardardowe

                print("Time of function {0} (runned {1} times): sum:{5}, avg:{2}, min:{3}, max:{4}".format(funName, count, caseAvg, caseMin, caseMax, caseSum))

                #TODO: times better
                times.append(caseAvg)



        print ("\n")
        
        #elapsedTime = None

        #print ("Time measure of function {0}: {1}".format(funName, elapsedTime))

        #TODO: what if millions of results?
        return (times, results)



class MeasureHelper:

    #TODO: maybe create class from this init function parameters?

    @autoassign
    def __init__(self, exactFun=None, compareFun=None, conf=None, getDataFromFile=None):
        """ Initialize class.
            :param conf: constants from exercise content
        """
        pass

    def setDataFiles(self, filenames):
        self.filenames = filenames

    #def getDataFromFile(filename):

    def measureFromFiles2(self):
        """ Measure time and mistakes on multiple files at once.
            Needed when one file contains one data case.
        """
        dataCases = []
        caseNames = self.filenames

        for filename in self.filenames:
            #Should return list of data cases (may be one)
            #TODO: multiple data cases per file
            dataCases.append(self.getDataFromFile(filename)[0])

        self.measureOnDataCases(dataCases, caseNames)



    # def measureFromFiles(self):
    #     for filename in self.filenames:
    #         #Should return list of data cases (may be one)
    #         dataCases = self.getDataFromFile(filename) 

    #         visFilename = filename + ".json"
    #         #TODO: convex hull
    #         self.measureOnDataCases(data, visFilename)


    def setAlgoFuns(self, algoFuns, algoFunNames):
        """ Set list of compared functions.
            :param algoFuns: list of functions
        """
        for i in range(len(algoFunNames)):
            #Assign names to functions
            algoFuns[i].__name__ = algoFunNames[i]

        self.algoFuns = algoFuns

    def measureOnDataCases(self, dataCases, visData, count=1):
        """ :param visData: filename or list of filenames (depends on algorithm (class?))
        """     
        print ("Data cases name(s):", visData) #visFilename
        print ("Data cases count: ", len(dataCases))
        #dataset - what if one data case
        #data - list of data cases


        #conf or param
        algo = AlgoMeasure(dataCases, conf=self.conf, exactFun=self.exactFun, compareFun=self.compareFun)

        #write results to field and return too
        #exactResults = 
        algo.computeResults() 


        times = []
        results = []

        #name could be in docstring?
        #name=
        #print (self.algoFuns)
        result = algo.measure(self.algoFuns[0], count=100)
        #TODO: how to assign to two arrays in one line?

        times.append(result[0])
        results.append(result[1])

        result = algo.measure(self.algoFuns[1], count=100)

        times.append(result[0])
        results.append(result[1])

        #(times[1], results[1]) = algo.measure(orient2dslow, name="orient2dslow")

        #compareTime = times[0]

        #return times compared to time of orient2dexact - for laboratory report
        #for time in times:
        #    print (compareTime/time)


        algo.compareResults(self.algoFuns[0].__name__, results[0])
        algo.compareResults(self.algoFuns[1].__name__, results[1])

        #TODO: visualization class; wizualizacja nie pasuje do klasy z pomiarami
        #algo.printVisualization(visFilename)

        print ("\n")
