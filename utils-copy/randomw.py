
import numpy as np

from mainutils import *
from geomutils import *
import polygon



def test_sphere():
    vectors = [PointGenerator.sphere() for i in range(1, 1000)]
    coords = np.array(vectors).T

    #plt.scatter(coords[0], coords[1]) #Projection for checking with eye if it seems good
    scatter3d(coords[0], coords[1], coords[2]) #it seems to work good

    plt.show(block=True)

class PointGenerator:
    @staticmethod
    def sphere():
        """ Generate uniformly distributed random point on sphere (2-sphere).
            :returns: one point (vector) 

        >>> np.allclose(np.linalg.norm(PointGenerator.sphere()), 1)
        True
        """
        #vec = PointGenerator.cube(3)
        vec = [(2*random.random()-1) for i in range(3)]
        dist = np.linalg.norm(vec)

        #For uniform distribution all layers which not contain full spheres must be decline
        while (dist > 1):
            vec = [(2*random.random()-1) for i in range(3)]
            dist = np.linalg.norm(vec)

        vec = normalize(vec)

        return vec

    @staticmethod
    def cuboid(size):
        """ Generate uniformly distributed random real point from n-dimensional range 0...size[i].
            :param size: vector of lengths of range (as list or numpy.array)
            :returns: n-dimensional vector (as numpy.array)

        >>> c = PointGenerator.cuboid([2, 2, 2])
        >>> np.greater_equal(c, [0, 0 ,0]).all()
        True
        >>> np.less_equal(c, [2, 2 ,2]).all()
        True
        """
        vec = map(lambda x: np.random.uniform(0, x), size)
        return np.array(vec)

    @staticmethod
    def cube(n):
        """ Generate uniformly distributed random real point in n-dimentional cube (1x1x...(n times)x1).
            :param n: dimension of cube
            :returns: one point (vector)

        >>> vec = PointGenerator.cube(2)
        >>> np.linalg.norm(vec, ord=np.inf) <= 1.0
        True
        >>> vec = PointGenerator.cube(5)
        >>> np.linalg.norm(vec, ord=np.inf) <= 1.0
        True
        """
        vector = []
        for i in range(0, n):
            vector.append(random.random())

        return vector



        

#TODO: maybe polygon factory?
class RandomPolygon:
	@staticmethod
	def ownMethod(vertCount):
		""" Generate random Polygon with own method.
			Known as variant0.

			Find random new vector from last point.
			When all point are found, change crossed point to no crossings:
				(as http://stackoverflow.com/questions/24970320/how-to-generate-random-polygons-in-python-that-do-not-have-bow-ties-intersectio).

			:param vertCount: Number of vertices of generated polygon.
			:returns: Polygon

            >>> ();polygon = RandomPolygon.ownMethod(5);() #doctest: +ELLIPSIS
            (...)
            >>> len(polygon.vertices)
            5
		"""
		point = np.array([0, 0])
		chain = [point]
		segments = []
		#TODO: another utility class - another file; only one method - randomPolygon

		for i in xrange(vertCount):
			print "Chain: ", chain

			b = True
			#TODO: refactor
			#end loop when new segment line doesn't crosses any segment lines
			while (b):
				move = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], 1)[0]
				print "Move: ", move
				point2 = point + move
				seg2 = LineSegment(point, point2)

				b = False
				# for seg in segments:
				# 	try:
				# 		if seg.crosses(seg2):
				# 			b = True
				# 			break;
				# 	except Exception as e:
				# 		print "Exception (seg.crosses): ", e
				# 		break;


			chain.append(point2)
			segments.append(seg2)

			# for seg1 in segments:
			# 	for seg2 in segments:
			# 		if seg.crosses(seg2):
						

			point = point2

		return polygon.Polygon(chain)


if __name__ == "__main__":

    import doctest
    doctest.testmod()

    #test_sphere()
