

import matplotlib.pyplot as plt
from itertools import islice
from itertools import cycle

#import flufl.enum
from enum import Enum
from collections import deque
import time

import randomw
from geomutils import *


#Don't add this - this is library module.
# plt.ion()
# plt.plot([2, 3], [1, 2])
# plt.plot([0, 1], [1, 0])
# plt.show()

# logfile = open("polygon.log", "w+")
#
# logging.basicConfig(stream=logfile, level=logging.DEBUG)
# log = logging.getLogger('Polygon')
# log.setLevel(logging.DEBUG)
# log.info('Polygon module')

class StepVisualization(EmptyVisualization):
    def __init__(self):
        #elements - collection of geometries (points, lines, faces etc.)
        #Geometries must have "__eq__", "__hash__" and plot.
        #If you want to add point - you must convert to Point outside before calling methods.
        self.elements = set()
        self.markedElements = {} #Element as key and configuration as value
        #self.markedColors = #map with default value - 'b'

        self.figure = plt.figure() # Figure()
        self.plt = self.figure.add_subplot(111)
        plt.show(block=False)

    def plot(self):
        #plt.clear()
        plt.clf()

        ContainerGeometry(list(self.elements), color='b').plot(plotter=self.plt)
        #maybe MarkedMixin?
        #TODO: different colors
        #Second container must be plotted after first
        ContainerGeometry(list(self.markedElements.keys()), conf=self.markedElements).plot(plotter=self.plt)

        plt.draw()
        time.sleep(0.2) #0.5

    @after(lambda x: x.plot())
    def add(self, element):
        self.elements.add(element)

    @after(lambda x: x.plot())
    def mark(self, element, color='r'):
        #TODO: mark should be structured?
        #self.markedColors[element] = color
        #el = copy(element)
        #el.color = color
        #self.elements.add(el)
        #self.elements[element].color = color
        self.markedElements[element] = {'color':color}

    @after(lambda x: x.plot())
    def unmark(self, element):
        try:
            print "unmark"
            print "markedElements: {}".format(self.markedElements)
            print "elements: {}".format(self.elements)
            #hack:
            self.markedElements[element] = {'color':'b'}
            self.markedElements.pop(element)
            print "unmarked"
            #el = copy(element)
            #el.color = StepVisualization.DEFAULT_COLOR

        except KeyError:
            print "KeyError - unmark"
            pass
            #log.info("unmark - tried to remove not existing point")

    @after(lambda x: x.plot())
    def remove(self, element):
        try:
            #TODO: don't remove, mark as gray?
            self.unmark(element)
            self.elements.remove(element)
        except KeyError:
            log.info("remove - tried to remove not existing point")

    def showSubsets(self, geometries):
        # for geometry in geometries:
        #     self.add(geometry)
        #     self.mark(geometry)
        #     self.remove(geometry)

        for geometry in geometries:
            self.add(geometry)
            self.mark(geometry)

class VisStack(deque):
    def __init__(self, vis, verts):
        self.vis = vis
        self.verts = verts

    def append(self, element):
        deque.append(self, element)
        #self.vis.mark(Point(self.verts[element]), color='g')
        self.vis.unmark(Point(self.verts[element]))

    def pop(self):
        result = deque.pop(self)
        #self.vis.unmark(result, color='g')
        self.vis.mark(Point(self.verts[result]), color='r')
        return result





VertType = Enum("VertType", 'start end split merge normal')

#Maybe all geometrical data inside objects should be nd.arrays?

class Triangulation:
    """ Class for triangulation of polygon.
        Private class?
    """

    def __init__(self, vertices):
        """ Initialize triangulation.
        :param vertices: initial vertices of triangulation (as np.array)
        """
        self.vertices = vertices
        self.triangles = []

    def __str__(self):
        triangles = self.getTrianglesAsPolygons()
        result = "Triangulation:"
        for triangle in triangles:
            result += "\n\t" + str(triangle)

        return result

    def addTriangle(self, indices):
        """ Add triangle to triangulation
        :param indices: indices of 3 triangle vertices
        """
        self.triangles.append(indices)

    @listify()
    def getTrianglesAsPolygons(self):
        for tr in self.triangles:
            triangleVertices = map(lambda x: self.vertices[x] ,tr)
            yield Polygon(triangleVertices)

#TODO: maybe Polygon interface?
class Polygon:
    """ Class representing 2D Polygon (simple - without holes).
    Polygon is designed for 2D (may not work for 3D).
    Polygon3D has another class.

    >>> polygon1 = Polygon(np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, 0.0]]))
    >>> polygon2 = Polygon(np.array([[-2.5, 4.5], [0.0, 5.0], [0.5, 4.0], [1.0, 3.0], [3.0, -1.0], [-3.0, 1.0], [-1.0, 2.0]]))
    >>> polygon3 = Polygon(np.array([[0, 0], [0, 2], [1, 4], [2, 2], [3, 4], [4, 2], [4, 0], [3, -2], [2, 0], [1, -2]]))
    >>> (polygon2.isYMonotone(), polygon3.isYMonotone())
    (True, False)
    >>> line1, line2 = polygon2.getYMonotoneLines()
    >>> print line1, line2
    >>> (np.allclose(line1[0], line2[0]), np.allclose(line1[-1], line2[-1])) #lines start in min and end in max
    (True, True)
    >>> (np.allclose(line1[0], [ 3.0,  -1.0 ]), np.allclose(line1[-1], [0.0, 5.0]))
    (True, True)
    >>> line1[1][0] < line2[1][0] #line1 is on the left
    True
    >>> polygon2.triangulate()
    >>> polygon4 = Polygon([[0.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, 0.0]]) #Test type conversion
    """
    #What is polygon? Is this only geometrical set, 
    #which has some properties, that we can return?
    #What with vertices order? We should preserve it when moved, because other class need it.

    def __init__(self, vertices):
        """ Simple polygon given with one 2D polygonal chain (vertices must be given in good order).
            May be clockwise or not.
            :param vertices: list of vertices,
                one vertex - np.array or list (converted internally to np.array)
        """
        assert(len(vertices[0])==2) #2D vertices
        #TODO: circular list? But what with export, when circular list?

        #FIXME: why was vertices, not np.array(vertices)?
        #self.vertices = vertices
        log.info("Polygon init - vertices: {}".format(vertices))
        self.vertices = map(np.array, vertices)
        #When use np.allclose?
        if np.allclose(self.vertices[0], self.vertices[-1]):
            del self.vertices[-1]

        vertices2 = list(self.vertices)
        vertices2.append(vertices2[0])  #Closed polygonal chain

        self.polyline = Polyline(vertices2)

    def __str__(self):
        return "Polygon({})".format(self.vertices)

    def getVertices(self):
        return self.vertices

    def moved(self, vector):
        """ Return new moved polygon.
            Order of vertices should be preserved (only moved by vector).
            :param vector: vector to move polygon (nd.array)
        """
        #If order of vertices will be not preserved, we will need VertexChain class
        return Polygon([vertex + vector  for vertex in self.getVertices()])



    @staticmethod
    def randomPolygon(params, variant=0):
        """ Return random polygon.
            :param variant: variant of method of generation of polygon (number)
            :param params: dictionary of parameters needed for this variant
        """

        
        if variant==0:
            #vertCount = params["vertCount"]
            #FIXME: not ended
            return randomw.RandomPolygon.ownMethod(params["vertCount"])

        #if variant==1:
        #   

        raise Exception("Wrong generation variant")




    @staticmethod
    def pointType(vPrev, v, vNext):
        #compare y
        b1 = vPrev[1] > v[1]
        b2 = vNext[1] > v[1]

        #if vPrev and vNext up from v
        if b1 and b2:
            if orient(vPrev, v, vNext):
                return VertType.split
            else:
                return VertType.start

        #if vPrev and vNext down from v
        elif (not b1) and (not b2):
            if orient(vPrev, v, vNext):
                return VertType.merge
            else:
                return VertType.end

        else:
            return VertType.normal


    def visualizePolygon(self, vis):
        """ Add polygon to visualizaton class as line segments and points.
        """
        for v in self.vertices:
            vis.add(Point(v))

        for segment in self.polyline.lines:
            vis.add(segment)

    def triangulate(self):
        """ Triangulate polygon. Return list of triangulation line segment.
            Input polygon must be y-monotone. Method doesn't check for this. 
            :returns: list of triangulation line segmentes 
        """

        if not self.isYMonotone():
            print "Algorithm only work for y-monotone polygon"
            return

        algoData = {}

        verts = np.array(self.vertices)

        #pdb.set_trace()
        #vis = EmptyVisualization()
        vis = StepVisualization()
        stack = VisStack(vis, verts) #deque()
        #FIXME: raise Exception("Matrix determinant: 0")

        self.visualizePolygon(vis)

        #trSegments = []
        triangulation = Triangulation(verts)

        (line1, line2) = self.getYMonotoneLinesIndices() #is line1 left?

        log.info("triangulate; monotone lines={}, {}".format(line1, line2))
        line2 = reversed(line2)

        #Now line1 is on the left
        log.info((line1, line2))

        side = [False]*len(self.vertices) #array(len(verts))

        #True - one side, False - another side
        for ind in line1:
            log.info(ind)
            side[ind]=True 
        #Others are on another side
        #for ind in line2:
        #    side[ind]=False 

        sortedInds = verts[:,1].argsort()


        #FIXME: is order good?
        stack.extend(sortedInds[:2])
        log.info("sortedInds: {}".format(sortedInds))

        algoData['vis'] = vis
        algoData['stack'] = stack
        algoData['triangulation'] = triangulation
        algoData['side'] = side
        algoData['verts'] = verts

        #loop throught all vertices
        for ind in sortedInds[2:]:
            self.findTrianglesBeforeVertex(ind, algoData)

        #FIXME: last segment may be side of polygon

        vis.showSubsets(triangulation.getTrianglesAsPolygons())

        #All vertices analysed, triangulation is ended
        return triangulation


    def findTrianglesBeforeVertex(self, ind, algoData):
        #invariant - all vertices in stack are from same side
        vis = algoData['vis']
        stack = algoData['stack']
        side = algoData['side']
        triangulation = algoData['triangulation']
        verts = algoData['verts']

        vis.mark(Point(verts[ind]), color='r')
        log.info((stack, ind))

        indLast = stack.pop()
        #vis.mark(indLast, color='r')

        if side[ind]!=side[indLast]:
            log.info("other side")
            indBeforeLast = stack.pop()
            stack.append(indBeforeLast)

            newTrLine = LineSegment(verts[ind], verts[indBeforeLast])
            triangulation.addTriangle([ind, indLast, indBeforeLast])
            vis.add(newTrLine)

            ind2=None
            while stack:
                #these vertices will not be removed from stack
                if ind2!=None:
                    vis.unmark(Point(verts[ind2]))

                ind2 = stack.pop()
                log.info(ind2)
                #vis.mark(ind2)

                try:
                    indBeforeLast = stack.pop()
                    stack.append(indBeforeLast)
                    newTrLine = LineSegment(verts[ind], verts[ind2])
                    triangulation.addTriangle([ind, ind2, indBeforeLast])
                    vis.add(newTrLine)
                except:
                    print "INFO: infBeforeLast not exists, stack is now empty"

            log.info (("to stack:", indLast, ind))
            #indLast - first(upper) vertex from another side of ind
            #vis.unmark(indLast)
            stack.append(indLast)
            stack.append(ind)
            #vis.mark(indLast, color='g')
            #vis.mark(ind, color='g')
            #Vis mark should change mark, when color is changed.
            #It should not stack marks?
            #When unmark normal color should stay if geometry is in normal visualization list.
            #TODO: maybe multiple lists in vis.add?

        else:
            log.info("same side")
            #if vertices from same side, we must analyze deeper
            indBeforeLast = stack.pop() #Stack will not be empty before this

            b2 = True
            while (b2):
                inds = np.array([indBeforeLast, indLast, ind])
                b = orient(*verts[inds])
                #xor
                if (b ^ side[ind]):
                    #without not result is wrong
                    #triangle inside polygon, triangulate
                    #we must analyze deeper (if stack not empty)
                    segment = LineSegment(verts[ind], verts[indBeforeLast])
                    vis.add(segment)
                    triangulation.addTriangle([ind, indLast, indBeforeLast])
                    #TODO: trSegments as class with visualization

                    vis.unmark(Point(verts[indLast]))
                    indLast = indBeforeLast

                    #If stack is empty, end loop
                    if (stack):
                        indBeforeLast = stack.pop()
                    else:
                        stack.append(indLast)
                        stack.append(ind)
                        #vis.mark(indLast, color='g')
                        #vis.mark(ind, color='g')
                        b2=False
                else:
                    #end loop
                    stack.append(indBeforeLast)
                    stack.append(indLast)
                    stack.append(ind)

                    b2=False #break



    #@log_with
    def plot(self, show=False, plotter=None, color=None):
        kwargs = {}

        if color!=None:
            kwargs['color'] = color

        log.info("Polygon - plot")
        #TODO: Polyline
        plotPoints = self.vertices
        plotPoints.append(self.vertices[0]) #cycle to first vertex
        plotPoints = np.array(plotPoints)

        plotCoordinates = plotPoints.T
        log.info("PlotCoordinates: {}".format(plotCoordinates))

        if plotter==None:
            plotter = plt

        plotter.plot(plotCoordinates[0], plotCoordinates[1], **kwargs)

        if show==True:
            plt.show()

    def getTypeOfVertices(self):
        """ Return types of vertices.
            :returns: list of enums (VertType)
        """
        #TODO: clockwise order of vertices is need for good classification
        #now points must be clockwise

        #Types of vertices
        vertTypes = []

        a = np.array(self.vertices)

        sortedInds = a[:,1].argsort()

        minInd = sortedInds[0]
        maxInd = sortedInds[-1]

        #prevVert = minVert

        #print minInd

        #vertices of polygon in starting from this with minimal y coordinate
        r = range(0, len(self.vertices)) #.rotate(minInd)

        firstInd = min(minInd, maxInd)
        secondInd = max(minInd, maxInd)

        #if (minInd<maxInd):
        line1 = r[firstInd+1:secondInd] #End before secondInd;
        line2 = (r[secondInd:] + r[:firstInd]) #reverse or not?; with firstInd or not? 

        verts = cycle(self.vertices)

        #rotatedInds = 

        #print VertType.start #minInd

        prevVert = next(verts)#self.vertices[minInd]
        vert = next(verts)
        nextVert = next(verts)

        #Cycle through all vertices
        for i in r:#line1: #xrange(minInd+1, len(self.vertices)):
            #print ("i", i)
            
            #vert = verts[i]
            #verts[i-1], vert, verts[i+1]           

            t = self.pointType(prevVert, vert, nextVert)

            print("vert: {0}, type: {1}".format(vert, t))
            print t

            vertTypes.append((vert, t))
            

            prevVert = vert
            vert = nextVert
            nextVert = next(verts)

        # for i in line2:
        #   #TODO: is this good (we go from up to down)?
        #   print ("i", i)
        #   vert = verts[i]

        #   t = self.pointType(verts[i-1], vert, verts[i+1])
        #   print t

        #np.sort(self.vertices, 1)
        #print (lines)
        return vertTypes

    #def verify(self): 

    def isYMonotone(self):
        """ Check if this polygon is y-monotone.
        """
        if self.getYMonotoneLines()==False:
            return False
        else:
            return True

    @Lazy
    def verts(self):
        #Lazy - polygon should be immutable
        return np.array(self.vertices)

    def getYMonotoneLines(self):
        lines = self.getYMonotoneLinesIndices()
        if lines==False:
            #Polygon is not y-monotone
            return False
        lineInd1, lineInd2 = lines

        return self.verts[lineInd1], self.verts[lineInd2]

    #TODO: maybe find monotony direction
    def getYMonotoneLinesIndices(self):
        """ Get two monotony lines if this polygon is y-monotone.
            Else return False. Points in lines are ordered from minimum to maximum.
            Lines contains min and max. First line is on the left.
            :returns: list of indexes of vertices of two monotony lines in order from minimal y; or False
        """
        a = np.array(self.vertices)
        #print a
        #Indexes of vertices sorted by y
        sortedInds = a[:,1].argsort()

        minInd = sortedInds[0]
        maxInd = sortedInds[-1]

        #print sortedInds

        #vertsByY = a[indsByY] #Sort by y; seems to work
        #minVert = vertsByY[0]

        #nonstrictly monotone?

        minVert = self.vertices[minInd]
        maxVert = self.vertices[maxInd]

        #print("maxInd", maxInd)

        #lines = [[minVert], [maxVert]]
        lines = [[], []]
        lineNr = 0

        prevVert = minVert

        #print minInd

        r = range(0, len(self.vertices)) #.rotate(minInd)
        rotatedInds = r[minInd:] + r[:minInd]

        for i in rotatedInds: #xrange(minInd+1, len(self.vertices)):
            #print ("i", i)
            vert = self.vertices[i]

            if i==minInd:
                lines[0].append(i)
                pass

            elif i==maxInd:
                lines[0].append(i)
                lines[1].append(i)
                lineNr = 1

            else:
                #print ("vert", vert)

                if lineNr==0:
                    #TODO: optimalization?; maybe only analize sortedInds?
                    if prevVert[1]>vert[1]:
                        #Polygon is not y-monotone on this line segment
                        return False

                else:
                    if prevVert[1]<vert[1]:
                        return False

                #Add to one of the chains
                #lines[lineNr].append(vert)
                lines[lineNr].append(i)

            prevVert = vert

        lines[1].append(minInd)

        #np.sort(self.vertices, 1)
        #print (lines)

        line1, line2 = lines
        line2 = list(reversed(lines[1]))

        #line1 must be on the left side
        if self.verts[line1][1][0] > self.verts[line2][1][0]:
            tmp = line1
            line1 = line2
            line2 = tmp

        return line1, line2

if __name__ == "__main__":
    import doctest
    doctest.testmod()