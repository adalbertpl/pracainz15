import Tkinter as tk
import ttk

import progressbar as pb


class ProgressBar(object):
    """ Progress bar abstract class.
        Show how much part of maximal value is actual progress.
        Class needed for BrownianMotion simulation.
    """
    #Progressbar without any action - EmptyProgressBar

    #TODO: setMaxValue method?

    def init(self, maxval):
        """ Initialize maximal value of progressbar.
            May not be called if value was set in constructor.
            Should work for every subclass (when run before other methods).
        """
        self.maxval = maxval

    def update(self, newValue):
        """ Update progress bar to new value.
        """
        pass

    def finish(self):
        """ Progress bar ended succesfully.
        """
        pass


EmptyProgressBar = ProgressBar


class TerminalProgressBar(ProgressBar):
    """ Progress bar in stdout terminal.
    """
    #it seems to work
    def __init__(self, maxval):
        self.init(maxval)

    def init(self, maxval):
        super(TerminalProgressBar, self).init(maxval)
        self.pbar = pb.ProgressBar(widgets=[pb.Percentage(), pb.Bar()], maxval=maxval).start()

    def update(self, newValue):
        self.pbar.update(newValue)

    def finish(self):
        #print "finish"
        self.pbar.finish()


class TkProgressBar(ProgressBar):
    def __init__(self, master, maxval):
        self.master = master
        self.init(maxval)

    def init(self, maxval):
        #looks enough good
        row = self.master.grid_size()[1]
        if self.master.progressbar!=None:
            row = row - 1 #Replace last progressbar

        label = tk.Label(self.master, text="Progress:")
        label.grid(row=row, column=0)

        self.maxval = maxval
        self.pbar = ttk.Progressbar(self.master, maximum=maxval)
        #print ("TkProgressBar", master)

        self.pbar.grid(row=row, column=1)
        #TODO: thread for computation?
        self.pbar.update_idletasks()
        self.master.progressbar = self

        self.oldValue = 0

    def update(self, newValue):
        self.pbar.step(newValue - self.oldValue)
        self.pbar.update_idletasks()
        self.oldValue = newValue

    def finish(self):
        #0.01 - as full progressbar - is this good for all cases?
        self.pbar.step(self.maxval - self.oldValue - 0.01)
        pass

