from __future__ import division

import os
import sys

if os.environ.get('USE_MY_UTILS_DEV_VERSION')=="1":
    sys.path.append(os.path.expanduser('~/AGH/Python/utils'))
    pass
elif os.environ.get('USE_MY_UTILS_DEV_VERSION')=="2":
    print "for this value of USE_MY_UTILS_DEV_VERSION program exit"
    raise Exception("Wrong USE_MY_UTILS_DEV_VERSION")
else:
    sys.path.append(os.path.expanduser('./utils-copy'))
    sys.path.append(os.path.expanduser('./utils'))
    #sys.path.append(os.path.expanduser('./utils-local'))
    pass

from collections import namedtuple
from itertools import combinations

import numpy as np

from geom3d import Range3d
from octree import ParticlesOctree

from mainutils import *
from geomutils import *

class Particles:
    def __init__(self, particles):
        """ Initialize Particles class.
        :param particles: np.array of particles positions
        """
        self.setPositions(particles)
        self.flyweights = [Particle(self.particles, i) for i in xrange(self.particlesNumber)]

    def getPositions(self):
        return self.particles

    def setPositions(self, positions):
        self.particles = positions
        try:
            self.particlesNumber = np.shape(particles)[0]
        except:
            self.particlesNumber = 0

    @property
    def objects(self):
        return (Particle(self.particles, i) for i in xrange(self.particlesNumber))

    def __getitem__(self, item):
        return Particle(self.particles, item)


class Particle:
    def __init__(self, particles, nr):
        self.nr = nr
        self.pos = particles[nr]
        self.particles = particles #Is this needed?
        #How to convert from list of Particles to numpy array

    @property
    def position(self):
        return self.pos
        #return self.particles[nr]

    def __repr__(self):
        return "{}".format(self.position)

    def __str__(self):
        return "{}".format(self.position)


class ParticleOld:
    #@autoassign
    #@contract(position='array[3]')
    def __init__(self, position):
        self.position = position

    def __repr__(self):
        return "{}".format(self.position)

    def __str__(self):
        return "{}".format(self.position)


class MeetFinderFactory(object):

    def __init__(self):
        self.conf = None

    def setConfiguration(self, conf):
        self.conf=conf

    @staticmethod
    def getName(meetfinder):
        #Maybe method in MeetFinder subclasses?
        if isinstance(meetfinder, BruteforceMeetFinder):
            return "bruteforce"
        elif isinstance(meetfinder, OctreeMeetFinder):
            return "octree"
        elif isinstance(meetfinder, CythonGridMeetFinder):
            return "grid"
        elif isinstance(meetfinder, GridMeetFinder):
            return "slowergrid"


    def createMeetFinder(self, name, initConf):
        """ Choose MeetFinder, initialize and configure it.
        :param conf: configuration for specific MeetFinder;
            but each MeetFinder should have default configuration values
        :param initConf: initialization parameters
        """
        #Selector or maybe Factory?
        #create(name, conf)?
        #Maybe static method in class MeetFinder? Or not?

        if name=="octree":
            meetFinder = OctreeMeetFinder(initConf["position"], initConf["size"])
        elif name=="grid":
            meetFinder = CythonGridMeetFinder(initConf["rMeet"])
        elif name=="slowergrid":
            meetFinder = GridMeetFinder(initConf["rMeet"])
        elif name=="bruteforce":
            meetFinder = BruteforceMeetFinder()
        else:
            raise Exception("Wrong MeetFinder name")

        if self.conf!=None:
            meetFinder.configure(self.conf)
            #Else default configuration should be applied

        return meetFinder

class MeetFinder(object):
    """ Interface for class responsible for finding a particle meeting.
    """

    def __init__(self):
        self.result = False #Variable will be true when result will be found
        self.step = None
        pass

    def clear(self):
        """ Run clear method for removing particles and reinitializing MeetFinder instance."""
        self.step = None
        pass

    def configure(self, conf):
        self.conf = conf
        #Is configuration always flat or sometimes hierarchical?

    def setMeetParams(self, rMeet):
        """ Configure MeetFinder. Set this before starting calculations."""
        self.rMeet = rMeet

    def initParticles(self, particles):
        """ Initialize particles position in meetings algorithm.
            Particles may be np.array or iterable of Particle(OldParticle)?
            Order of particles must be unchanged from start to end of simulation.
            :param particles: Particles from simulation with set start positions."""
        #Or maybe particles should be np.array?
        #self.particlesObject = Particles(particles)
        self.particles = particles

    def moveParticles(self, particles, newPositions):
        """ This function may only move particles locally.
            Function needed for changing state of algorithm.
            Order of particles and order of positions must be unchanged from start to end of simulation.
            Particles in simulation should be changed after this method. """
        # rowCount = np.shape(particles)[0]
        # for i in range(rowCount):
        #     self.moveParticle(self.particlesObject[i], newPositions[i])

        for i in xrange(len(particles)):
            self.moveParticle(self.particles[i], newPositions[i])


    def newStep(self, step):
        """ Run this on new step. Run before first step.
            :param step: step=lastStep+1"""
        self.step = step
        #Search for meeting here or in foundMeeting?
        #Maybe it will depend on implementation?
        #Default behaviour - may be overrided

    def moveParticle(self, particle, newPosition):
        """ Move particle from actual to new position."""
        #Default action; maybe override
        particle.position = newPosition

    def foundMeeting(self, force=False):
        """ Return True if meeting was found by now and we can stop computation.
        :param force: force meetings to be calculated now (synchronously).
        """
        #Computation can be done asynchronously for better performance?
        #Maybe multi-core or GPU?
        pass

    def getFirstMeetingStep(self):
        """ Return step number of first meeting if first meeting was on step <=actualStep.
            Else return None.
        """
        #n Python 0 == False
        if self.foundMeeting(force=True):
            return self.firstMeetingStep
        else:
            return None

    #Inversion of methods?
    #Private method
    #def simulateStep(self):

    def checkAllMeetings(self, particles):
        """ Return True if any meeting between particles was found.
            :param particles: list[Particle]
        """
        #May be needed for some implementations (e.g. searching meetings in one box(node) of tree).
        #loop by all different pairs (combinations)
        for (p1, p2) in combinations(particles, 2):
            #TODO: maybe change this line (performance)?
            #Combinations - not needed
            #if p1==p2:
            #    continue

            if self.meets(p1, p2):
                return True

        return False

    def meets(self, p1, p2):
        """ Return True if two particles collide, otherwise return False.
        """
        #return (np.linalg.norm(p2-p1) < self.rMeet)
        return (np.linalg.norm(p2.position-p1.position) < self.rMeet)


# Not works
# class defaultnamedtuple(namedtuple):
#     def __init__(self, name, fields, defaults):
#         super(defaultnamedtuple, self).__init__(name, fields)
#         self.setdefaults(defaults)
#
#     def setdefaults(self, defaults):
#         self.__new__.__defaults__ = defaults

def checkAllMeetings(particles, rMeet):
    """ Return True if any meeting between particles was found.
        :param particles: np.array of particles positions
    """
    #Maybe class?
    #Maybe instatiate MeetFinder class?
    #Similar function is in MeetFinder
    #TODO: this function should be in cython
    #Version for reflective border condition
    #May be needed for some implementations (e.g. searching meetings in one box(node) of tree).
    #loop by all different pairs (combinations)
    for (p1, p2) in combinations(particles, 2):
        #TODO: maybe change this line (performance)?
        #Combinations - not needed
        #if p1==p2:
        #    continue

        if meets(p1, p2, rMeet):
            #return (p1, p2)
            return True

    return False

def meets(p1, p2, rMeet):
    """ Return True if two particles collide, otherwise return False.
    """
    #return (np.linalg.norm(p2-p1) < self.rMeet)
    #TODO: this function should be in cython
    return (np.linalg.norm(p2-p1) < rMeet)


def checkGridMeetings(particles, gridLocation, rMeet):
    #zipped = np.vstack((gridLocation, particles)) #np.dstack
    #zipped = np.sort(zipped) #FIXME
    #sortind = np.argsort(gridLocation, axis=0)[:,0]
    sortind = np.lexsort(np.transpose(gridLocation)[::-1])

    oldGrid = gridLocation[sortind[0]] - [2, 2, 2] #Smaller that all grids (partial order)
    #TODO: oldGrid may be not smaller that all grids


    count = np.shape(particles)[0]
    i = 0

    oldGridRow = 0
    oldGrid = gridLocation[sortind[i]]

    i += 1
    while i<count:
        #row = zipped[i]
        #particle = particles[sortind[i]]
        particleGridLocation = gridLocation[sortind[i]] #row[1]

        if (np.any(oldGrid != particleGridLocation)):
            #Check all meetings for particles from one grid box
            #
            result = checkAllMeetings(particles[sortind[oldGridRow:i]], rMeet)
            if result!=False:
                return result

            oldGrid = particleGridLocation
            oldGridRow = i

        i += 1

    result = checkAllMeetings(particles[sortind[oldGridRow:]], rMeet)
    if result!=False:
        return result


    return False

#TODO: AdaptativeMeetFinder (different number of particles - different grid finder)

#Import module compiled from Cython
#Import module compiled from Cython
import meetFinderCython

class ParticlesGrid:
    def __init__(self, particles, rMeet):
        """

        :param particles: np.array of particles positions?
        :param rMeet: radius of particles meeting (in meters)
        :return:
        """
        self.rMeet = rMeet

        #TODO: what step is sufficient?
        zero = np.array([0,0,0])
        step = np.array([1, 1, 1])*(rMeet)*10 #rMeet is here in meters
        self.grid = [None]*4
        self.grid[0] = Grid(zero, step)
        self.grid[1] = Grid(zero + step/4, step)
        self.grid[2] = Grid(zero + 2*step/4, step)
        self.grid[3] = Grid(zero + 3*step/4, step)

        self.particlesGrid = [None] * 4

        self.particles = particles
        for i in range(0, 4):
            self.particlesGrid[i] = self.findGrids(particles, i)


    def moveParticles(self, particles, newPositions):
        #particles.position = newPositions
        self.particles = newPositions

        for i in range(0, 4):
            self.particlesGrid[i] = self.findGrids(newPositions, i)

    def checkMeetings(self):
        #result1 = checkAllMeetings(self.particles, self.rMeet)
        #print "CheckAllMeetings: {}".format(result1)

        for i in range(0, 4):
            b = checkGridMeetings(self.particles, self.particlesGrid[i], self.rMeet)
            if b!=False:
                return True

        return False



    def findGrids(self, newPositions, gridNumber):
        #coordinates - integer coordinates
        coordinates = self.grid[gridNumber].fromPositions(newPositions)
        return coordinates

    # def add(self, particles):
    #     pass
    #
    # def findbbox(self, range3d):
    #     pass


class ParticlesGridCython(ParticlesGrid):
    #def __init__(self, particles, rMeet):
    #    super(ParticlesGridCython, self).__init__(particles, rMeet)

    def checkMeetings(self):
        #result1 = checkAllMeetings(self.particles, self.rMeet)
        #print "CheckAllMeetings: {}".format(result1)

        for i in range(0, 4):
            b = meetFinderCython.checkGridMeetings(self.particles, self.particlesGrid[i], self.rMeet)
            #b2 = checkGridMeetings(self.particles, self.particlesGrid[i], self.rMeet)
            #if b!=b2:
            #    print "Error - checkGridMeetings - Cython version different result - Cython:{}, default:{}".format(b, b2)
            if b!=False:
                return True

        return False


class GridMeetFinder(MeetFinder):
    def __init__(self, rMeet):
        super(GridMeetFinder, self).__init__()
        self.rMeet = rMeet

    #def configure(self, conf):

    def initParticles(self, particles):
        #TODO:check this
        #super(GridMeetFinder, self).initParticles(particles)
        self.particles = particles[0].particles #np.array(particles)
        #print (particles, type(particles))
        self.grid = ParticlesGrid(self.particles, self.rMeet)

    def moveParticles(self, particles, newPositions):
        self.grid.moveParticles(particles, newPositions)

    def foundMeeting(self, force=False):
        if self.result == False:
            return False
        else:
            return True

    def newStep(self, step):
        super(GridMeetFinder, self).newStep(step)

        self.result = self.grid.checkMeetings()
        if self.result:
            self.firstMeetingStep = step

#Inheritance may be a problem - isinstance(GridMeetFinder)
class CythonGridMeetFinder(GridMeetFinder):
    """ Enhanced GridMeetFinder (better performance with use of Cython)
    """
    #def __init__(self, rMeet):
    #    super(CythonGridMeetFinder, self).__init__(rMeet)

    def initParticles(self, particles):
        #TODO:check this
        #super(GridMeetFinder, self).initParticles(particles)
        self.particles = particles[0].particles #np.array(particles)
        #print (particles, type(particles))
        self.grid = ParticlesGridCython(self.particles, self.rMeet)

OctreeMeetFinderConf = namedtuple_with_defaults("OctreeConf", "minCount maxCount", (4, 8))

#Something is wrong because statistics are wrong?
class OctreeMeetFinder(MeetFinder):
    #TODO: check if time of meeting from two meetFinders have same probability distribution
    def __init__(self, position, size):
        super(OctreeMeetFinder, self).__init__()

        #TODO: Set parameters inside runOneSimulation or clear method?
        #Maybe not constructor but init method?

        self.position = position
        self.size = size

        self.firstMeetingStep = None

        self.configure(OctreeMeetFinderConf())
        self.clear()

    def configure(self, conf):
        assert(isinstance(conf, OctreeMeetFinderConf)) #assertion or exception?
        #Maybe we should here create namedtuple from tuple/dict?
        self.conf = conf

    def clear(self):
        super(OctreeMeetFinder, self).clear()
        self.octree = ParticlesOctree(self.position, self.size, minCount=self.conf.minCount, maxCount=self.conf.maxCount) #New octree without particles        self.firstMeetingStep = None

    def initParticles(self, particles):
        #FIXME
        super(OctreeMeetFinder, self).initParticles(particles) #Hack?
        map(self.octree.add, particles)

    def moveParticle(self, particle, newPosition):
        self.octree.moveParticle(particle, newPosition)

    #TODO: check if findMeeting is checked after end of simulation
    def foundMeeting(self, force=False):
        if self.firstMeetingStep!=None:
            return True
        else:
            return False

    def newStep(self, step):
        super(OctreeMeetFinder, self).newStep(step)

        for box in self.octree.leafs:
            #if self.checkAllMeetings(box.particles):
            for particle in box.particles:
                boxes = self.octree.findbbox(Range3d.create(center=particle.position, radius=self.rMeet))
                for box2 in boxes:
                    for p2 in box2.particles:
                        if particle!=p2 and self.meets(particle, p2):
                            self.firstMeetingStep = self.step
                            return

        #No meeting was found
        return False


class BruteforceMeetFinder(MeetFinder):
    def __init__(self):
        super(BruteforceMeetFinder, self).__init__()
        pass

    def initParticles(self, particles):
        #super(BruteforceMeetFinder, self).initParticles(particles)
        self.particles = particles

    def foundMeeting(self, force=False):
        if self.result == False:
            return False
        else:
            return True

    def newStep(self, step):
        super(BruteforceMeetFinder, self).newStep(step)
        self.result = self.checkAllMeetings(self.particles)

        if self.result != False:
            self.firstMeetingStep = self.step
