#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import os
import sys
import time
import tkFileDialog
from Tkconstants import SUNKEN, W, E, RIGHT, Y

from meetfinder import GridMeetFinder, CythonGridMeetFinder

if os.environ.get('USE_MY_UTILS_DEV_VERSION')=="1":
    sys.path.append(os.path.expanduser('~/AGH/Python/utils'))
elif os.environ.get('USE_MY_UTILS_DEV_VERSION')=="2":
    print "for this value of USE_MY_UTILS_DEV_VERSION program exit"
    raise Exception("Wrong USE_MY_UTILS_DEV_VERSION")
else:
    sys.path.append(os.path.expanduser('./utils-copy'))
    sys.path.append(os.path.expanduser('./utils'))
    #sys.path.append(os.path.expanduser('./utils-local'))

import Tkinter as tk
import tkMessageBox
import ttk
#from Tkinter import ttk

import ast
from matplotlib import pyplot

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np

from particles import BrownianMotion, UserSimRunner, ParticlesSimVis, SimRunner, SimConfig, OctreeMeetFinder, \
    BruteforceMeetFinder, ParticlesJsonEncoder, ParticlesJsonDecoder, ValueVectorGenerator


class Application(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        def on_closing():
            #if tkMessageBox.askokcancel("Exit", "Do you want to exit?"):
            pyplot.close('all')
            master.destroy()

        master.protocol("WM_DELETE_WINDOW", on_closing)

        #self.plotFrame = plotFrame

        #TODO: refactor (automatically add units?)
        #fieldLabels = ["stepTime [s]", "simTime [s]", "rangeX [m]", 
        #    "rangeY [m]", "rangeZ [m]", "minSpeed [mm/s]", 
        #    "maxSpeed [mm/s]", "rMeet [mm]"]

        fieldLabels = ["stepTime [s]", "simTime [s]", 
            "range list [m]", 
            "minSpeed [mm/s]", 
            "maxSpeed [mm/s]", "rMeet [mm]",
            "particlesNumber"]

        fieldNames = ["stepTime", "simTime",
            "rangeList",
            "minSpeed",
            "maxSpeed", "rMeet", "particlesNumber"]

        defaultValues = BrownianMotion.exampleParams()

        self.fields = zip(fieldLabels, fieldNames, defaultValues)
        self.plotNumber = 0
        self.simVisNewWindow = ParticlesSimVis("newWindow")

        #Important simulation data history
        #Should this be here or in UserSimRunner?
        #Should UserSimRunner be created for every multiple simulation?
        self.simData = None
        self.simDataList = []
        self.startPosition = "random"
        self.generatorValues = None

        self.advancedResults = tk.IntVar(0)
        self.dbSave = tk.IntVar(0)
        self.particlesAnimation = tk.IntVar(0)

        #Fragment based on http://tkinter.unpythonic.net/wiki/tkFileDialog
        self.fileOptions = opt = {}
        opt['defaultextension'] = '.json'
        opt['filetypes'] = [('all files', '.*'), ('simulation result files', '.json')]

        self.grid()
        self.createWidgets()

    #@property
    #def mainRowCount(self):
    #    return self.grid_size()[1]

    def addForm(self):
        self.entries = []

        (colCnt, rowCnt) = self.grid_size()

        for (fieldLabel, fieldNames, defVal) in self.fields:
            rowCnt+=1

            #TODO: refactor
            row = self
            lab = tk.Label(row, width=15, text=fieldLabel, anchor='w')

            #v = IntVar()
            ent = tk.Entry(row)
            ent.insert(0, defVal)


            #row.grid()
            #TODO: same row
            lab.grid(row=rowCnt, column=0) #, padx=5, pady=5)
            ent.grid(row=rowCnt, column=1)
            #will not work:
            #row.pack(side=tk.TOP, fill=tk.X, padx=5, pady=5)
            #lab.pack(side=tk.LEFT)
            #ent.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)

            #TODO: maybe field class?
            #self.entries[fieldName] = ent
            self.entries.append(ent)

    def addEntryWithLabel(self, label, defVal=0, entryType=None):
        if entryType==None:
            entryType = tk.StringVar()


        (colCnt, rowCnt) = self.grid_size()

        row = self
        lab = tk.Label(row, width=15, text=label, anchor='w')

        #ent = tk.Entry(row)
        #TODO: change function signature
        ent = tk.Spinbox(row, from_=1, to=10000)
        #ent.insert(0, defVal)

        lab.grid(row=rowCnt, column=0)
        ent.grid(row=rowCnt, column=1)

        return ent


    def createWidgets(self):

        self.menubar = tk.Menu(self)
        self.menubar.add_command(label="Help", command=self.help)
        self.menubar.add_command(label="Exit", command=self.quit)
        self.menubar.add_command(label="Advanced", command=self.advancedOptions)
        # display the menu
        self.master.config(menu=self.menubar)


        self.addForm()

        ttk.Separator(self, orient=tk.HORIZONTAL)
        #ttk.grid()
        #s.grid()

        self.times = self.addEntryWithLabel("times", 1)
        #If times change from one, visualization should be automatically unticked?
        #It will need to be changed by user

        lab = tk.Label(self, width=15, text="meetFinder", anchor='w')
        lab.grid(row=9, column=0)

        self.meetFinderVar = tk.StringVar(self)
        self.meetFinderVar.set("BruteforceMeetFinder")
        self.meetFinderMenu = tk.OptionMenu(self, self.meetFinderVar, "GridMeetFinder", "BruteforceMeetFinder", "OctreeMeetFinder", "SlowerGridMeetFinder")
        self.meetFinderMenu.grid(row=9, column=1)

        self.plotWindow = tk.IntVar(0)
        self.windowPlace = tk.Checkbutton(self, text="Plot in this window", variable=self.plotWindow)
        #self.windowPlace.grid()

        #TODO: option - don't show visualization (vis.turnOffVisualization()?)

        label = tk.Label(self, text="Result:", height=2, anchor="s")
        label.grid(columnspan=3)

        frame = tk.Frame(self, bd=2, height=5, width=20)
        frame.grid(columnspan=3)

        self.scrollbar = tk.Scrollbar(frame)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.text = tk.Text(frame, height=5, width=20)
        #self.text.config(state=tk.DISABLED) #Result will not show
        self.text.pack()

        self.text.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.text.yview)

        counterFrame = tk.Frame(self, height=1)
        counterFrame.grid(columnspan=2)

        #Refactor - do we need so many variables?
        #TODO: change counters, when multiple-simulation is run
        self.meetCounter = 0
        self.notMeetCounter = 0
        self.meetCounterVar = tk.IntVar(0)
        self.notMeetCounterVar = tk.IntVar(0)
        self.meetCounterLabel = tk.Label(counterFrame, textvariable=self.meetCounterVar, relief=SUNKEN)
        #row = self.grid_size()[1] + 1
        label = tk.Label(counterFrame, text="Meet, not meet:")
        label.grid(row=0, column=0)
        self.meetCounterLabel.grid(row=0, column=1, padx=5)
        self.notMeetCounterLabel = tk.Label(counterFrame, textvariable=self.notMeetCounterVar, relief=SUNKEN)
        self.notMeetCounterLabel.grid(row=0, column=3, padx=5)

        import tkFont
        helv = tkFont.Font(family='Helvetica',
        size=9)
        self.resetButton = tk.Button(counterFrame, text="Reset", command=self.resetCounters, width=5, font=helv)
        self.resetButton.grid(row=0, column=4)

        #Tabs for program plots
        #TODO: better layout after plot will show

        #self.plotNotebook.grid(column=3, row=1, columnspan=2, rowspan=5)
        #self.plotNotebook.grid()
        #self.plotFrame = plotNotebook

        #self.frame = tk.Frame(self)
        #self.frame.grid()

        #self.e1 = tk.Entry(self)
        #self.e1.grid()
        
        self.runButton = tk.Button(self, text='Start simulation', command=self.run, fg="#FF0000") #bg="#C0C0EE"
        self.runButton.grid(columnspan=3)

        #TODO: Stop simulation button.

        self.progressbar = None

        #self.rightSide = tk.Frame(self, width=600, height=450)
        #self.plotNotebook = ttk.Notebook(self)
        #self.plotNotebook.grid(column=3, row = 0, rowspan = self.grid_size()[1])
        self.rightSide = tk.Frame(self)
        #self.rightSide.grid()
        self.rightSide.grid(column=3, row = 0, rowspan = self.grid_size()[1])
        #self.progressbar = TkProgressbar(self, 100)
        #self.defaultRunButton = tk.Button(self, text='', command=self.run) #self.quit
        #self.defaultRunButton.grid()
        #Maybe showPlotButton ?
        
        # self.master.bind('<Return>', self.runInstruction)

    def resetCounters(self):
        #Reset simulations - all simulation data that is not saved is deleted
        #Reset counters to count again from 0
        self.meetCounter = 0
        self.notMeetCounter = 0
        self.meetCounterVar.set(self.meetCounter)
        self.notMeetCounterVar.set(self.notMeetCounter)

        self.simDataList = []
        self.simData = None

    def advancedOptions(self):
        window = tk.Toplevel(self)
        window.wm_title("Advanced options")
        name = tk.Label(window, text="Advanced options", anchor='w')
        name.pack(side="top")

        button = tk.Button(window, text="Set start position", command=self.openStartPositionFile)
        button.pack()

        self.startPositionFilenameVar = tk.StringVar()
        self.startPositionFilename = tk.Label(window, textvariable=self.startPositionFilenameVar)
        self.startPositionFilename.pack()

        button = tk.Button(window, text="Set generator values", command=self.openGeneratorValuesFile)
        button.pack()

        self.generatorValuesFilenameVar = tk.StringVar()
        self.generatorValuesFilename = tk.Label(window, textvariable=self.generatorValuesFilenameVar)
        self.generatorValuesFilename.pack()

        checkbutton = tk.Checkbutton(window, text="Remember advanced results", variable=self.advancedResults)
        checkbutton.pack()

        button = tk.Button(window, text="Save simulation result to file", command=self.saveResult)
        button.pack()

        button = tk.Button(window, text="Save all results to file ({})".format(self.meetCounter+self.notMeetCounter), command=self.saveAllResults)
        button.pack()

        checkbutton = tk.Checkbutton(window, text="Particles animation", variable=self.particlesAnimation)
        checkbutton.pack()

        checkbutton = tk.Checkbutton(window, text="Remember new simulation results in database", variable=self.dbSave)
        checkbutton.pack()

        button = tk.Button(window, text="Create database for results", command=self.createResultsDatabase)
        button.pack()

    def createResultsDatabase(self):
        info = UserSimRunner.createDatabase()
        if info==0:
            tkMessageBox.showinfo("Database already exists", "Database already exists")
        else:
            tkMessageBox.showinfo("Database created", "Database created")

    def saveAllResults(self):
        print "Not implemented"

    def saveResult(self):
        #http://stackoverflow.com/questions/19476232/save-file-dialog-in-tkinter
        f = tkFileDialog.asksaveasfile(mode='w', defaultextension=".json")
        if f is None:
            return

        print self.simData
        jsonText = ParticlesJsonEncoder().encode(self.simData) #self.simDataList[-1]
        f.write(jsonText)
        f.close()

    def openStartPositionFile(self):
        #x, y = self.winfo_x(), self.winfo_y()
        goodFile = False
        try:
            openedFile = tkFileDialog.askopenfile(mode='r', **self.fileOptions)
            simData = json.load(openedFile)
            if 'startPosition' in simData:
                goodFile = True
                self.startPosition = ParticlesJsonDecoder.toNumpy(simData["startPosition"])
                self.startPositionFilenameVar.set(openedFile.name)
                #When start position is set, some parameters (as particles number) are not used
        except ValueError, AttributeError:
            #File is not good
            pass

        if goodFile==False:
            tkMessageBox.showwarning("Wrong file", "Selected file doesn't contain properly formatted data")

    def openGeneratorValuesFile(self):
        #It seems to work
        goodFile = False
        try:
            openedFile = tkFileDialog.askopenfile(mode='r', **self.fileOptions)
            simData = json.load(openedFile)
            if 'generatorResults' in simData:
                goodFile = True
                self.generatorValues = ParticlesJsonDecoder.toNumpy(simData["generatorResults"])
                self.generatorValuesFilenameVar.set(openedFile.name)
                #When start position is set, some parameters (as particles number) are not used
        except ValueError, AttributeError:
            #File is not good
            pass

        if goodFile==False:
            tkMessageBox.showwarning("Wrong file", "Selected file doesn't contain properly formatted data")

    def run(self):
    	""" Set parameters and run simulation.
    	"""
        print("GUI.run()")

    	#TODO: set parameters
    	#params = BrownianMotion.exampleParams()
        #It seems to work
        params1 = map(lambda ent: ast.literal_eval(ent.get()),self.entries)
        params = SimRunner.flatParamsFormat(params1)
        print(params)
        
        if self.plotWindow.get():
            #gui=self.rightSide
            gui=self
            simVis = ParticlesSimVis(gui)
        else:
            #What with particlesAnimation checkbox?
            gui="newWindow"
            simVis = self.simVisNewWindow



        #self.progressbar = ttk.Progressbar(self, maximum)

        times = int(self.times.get())

        args = params
        #rMeet = params[7]
        meetFinderVar = self.meetFinderVar.get()
        if meetFinderVar=="BruteforceMeetFinder":
            meetFinder = BruteforceMeetFinder()
        elif meetFinderVar=="OctreeMeetFinder":
            meetFinder = OctreeMeetFinder(np.array([0,0,0]), np.array([args.rangeX, args.rangeY, args.rangeZ]))
        elif meetFinderVar=="GridMeetFinder":
            #print "GridMeetFinder"
            meetFinder = CythonGridMeetFinder(params.rMeet)
        elif meetFinderVar=="SlowerGridMeetFinder":
            meetFinder = GridMeetFinder(params.rMeet)
        else:
            raise Exception("Wrong meetFinderVar")


        #TODO: parametr wizualizacji np. do pliku
        #maybe use "visual" for progressbar too instead of specifying "pbPlace"?
        if times==1:
            simConfig = SimConfig(withPlots=True)
    	    #self.brownianMotion = BrownianMotion(*params, visual=gui, progressbar=True, pbPlace=self) #visual=self
            runner = UserSimRunner(params, times, simConfig, pbPlace=self, advancedResults=self.advancedResults.get(),
                                   startPosition=self.startPosition, dbSave=self.dbSave.get(), animation=self.particlesAnimation.get())
            runner.setSimVisualization(simVis) #Maybe change it?
            args = params
            runner.setMeetFinder(meetFinder)

            if self.generatorValues!=None:
                generator = ValueVectorGenerator(self.generatorValues)
                runner.setGenerator(generator)

            self.simData = runner.run()[-1]
            self.simDataList.append(self.simData)

            if self.simData["result"]!=False:
                self.meetCounter += 1
            else:
                self.notMeetCounter += 1
    	    #result = self.brownianMotion.simulate()
            #print (str(result))

    	    #self.text.insert(tk.START, "{}\n".format(simData.result))
            self.text.insert(tk.END, "{}\n".format(self.simData["result"]))

        else:
            simConfig = SimConfig(withPlots=False)
            #meetCnt = runMultipleTimes(params, times, progressbar=True, pbPlace=self, visual=None)

            #print result
            runner = UserSimRunner(params, times, simConfig, pbPlace=self, startPosition=self.startPosition,
                                   dbSave=self.dbSave.get(), animation=self.particlesAnimation.get())
            runner.setSimVisualization(ParticlesSimVis(gui)) #Maybe change it?

            runner.setMeetFinder(meetFinder)

            if self.generatorValues!=None:
                generator = ValueVectorGenerator(self.generatorValues)
                runner.setGenerator(generator)

            self.simDataList = self.simDataList + runner.run()

            result = (runner.meetCount, times-runner.meetCount)
            self.meetCounter += result[0]
            self.notMeetCounter += result[1]

            self.text.insert(tk.END, "{}\n".format(result))

        self.meetCounterVar.set(self.meetCounter)
        self.notMeetCounterVar.set(self.notMeetCounter)

        tk.mainloop()

        #canvas.grid()

    def plot(self, fig):
        """ Plot Matplotlib Figure inside Tkinter.
            Function will be run from SimVisualization.
            This function is not used if plot is done in Matplotlib window.
            :param fig: Figure to plot
        """
        self.plotNumber+=1

        #frame = tk.Frame(self.plotNotebook)
        frame = self.rightSide
        #self.plotNotebook.add(frame, text="Plot {0}".format(self.plotNumber))

        canvas = FigureCanvasTkAgg(fig, master=frame)
        canvas.show()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)


    def help(self):
        helpMessage = \
        """ Particles Program.
            Simulation of brown motion of group of particles.

            If program is configured to plot in windows then
                result plot cannot be rotated and zoomed.
        """

        #""" Symulacja ruchów Browna grupy cząstek.
        #"""
        tkMessageBox.showinfo("Pomoc", helpMessage)

        
        
    # def runInstructionFromHistory(self, instructionText):
    #     result = self.calc.parseInstruction(instructionText);
    #     self.text.insert(tk.END, result)
        
    # def getHistoricalInstruction(self, *args):
    #     instructionNumber = int(self.e1.get())
    #     result = self.calc.getHistoricalInstruction(instructionNumber)
        
    #     answer = tkMessageBox.askquestion("Run historical instruction?", "Instruction: {0}".format(result))
    #     if (answer==tkMessageBox.YES):
    #         self.runInstructionFromHistory(result)

def createApplication(root):
    app = Application(root)
    app.master.title('Particles')
    app.pack()
    return app

#TODO: maybe range as separated text fields?
def main():
    #print sys.argv
    #plotTabs = (len(sys.argv)>1) and (sys.argv[1]=="tabPlots") #Why this is set?
    root = tk.Tk()
    app = createApplication(root)
    root.mainloop()
    return app


if __name__ == "__main__":
    main()