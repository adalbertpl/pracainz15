Project Particles
=================

## Project short description
Main part of project is program used for running multiple particles random walk 3D simulation.

Simulation return time when first particles meeting (collision) occured.  
Simulation runs until first collision or until time reaches maximum simulation time.  
Extension of project is program statistics.py used for computing time and probability statistics and displaying plots.

Program has two versions (command-line and GUI) which usage is described in this document.  
Program can plot 3D visualization of tracks of particles.

If you want check usage of program jump to Prerequisites section.

## Project purpose
Program will be used for testing hipothesis related to planktonic microorganisms.

## Program details
Program implements three types of MeetFinders (class for finding particles meetings) for better performance.

For efficiency some parts of program are implemented using Cython.
Program implements octree data structure.
Particles are reflecting from space borders (cuboid).
Program displays progress bar.

Simulation results can be saved to file from GUI (also with generated random vectors and with start positions). This was used for testing correctness of MeetFinders.

Correctness of MeetFinders was checked with simulationchecker.py (run with: python ./simulationchecker.py). MeetFinders give same results (for same generator random vectors and same start position). 

Visulization from program:
![Particles tracks](https://bytebucket.org/wojtek132/pracainz15/raw/616081854e1695d0b1ab11a594538bc78650507b/vis/Visualization3D-4.png)

### Addon - Statistics program
With this program you can check on plot i.e. how depends meet probability on different simulation parameters (e.g. radius of meeting).
This is an example how researcher can use this program.

![Visualization](https://bytebucket.org/wojtek132/pracainz15/raw/da8a545f334b4c318ef9730f8a07f99899ce4299/vis/MeetProbabilityTorMeet-1000repeats-1-3600-3-3-3-10-100-x-2particles.png)

You can check also how performance depends on choosen MeetFinder.
It will be seen that BruteforceMeetFinder has quadratic complexity and that OctreeMeetFinder has n*log(n) complexity and is faster from about 30 particles.
It will be seen also that GridMeetFinder using Numpy and Cython is fast.

## Prerequisites
Linux:  
Python 2.7 must be installed. Pip must be installed.  
Numpy and Matplotlib must be installed.  
Install other dependencies with ./linux-installer.sh command

Windows:  
Python 2.7 must be installed. Pip must be installed.  
Numpy and Matplotlib must be installed.  
For best experience install Python distribution (Anaconda, WinPython or PythonXY).  
Install other dependencies with ./windows-installer.bat (from cmd).

Recommendation for Windows:  
Install Anaconda with default options (e.g. Anaconda 2.5.0)  
Anaconda has Numpy and Matplotlib installed.  
Next run Windows cmd or Powershell and  
run ./windows-installer.bat from main program folder.

More about prerequisites - see user documentation (pdf).



# Usage

## First version - command line:
Program returns result to stdout  
-t option:  
	Add -t option for counting mode (run multiple times and count meetings).  
	Program will return two numbers (meetings count, without-meetings count).  
Add -v option for debug mode (with visualization).  
Without argument program run with default values (always with visualization).  
Arguments are:  
  * stepTime [s],   
  * simulation time [s],  
  * X length of cuboid [m],  
  * Y length of cuboid [m],  
  * Z length of cuboid [m],  
  * minimal speed of one step move [mm/s],  
  * maximal speed of one step move [mm/s],  
  * radius of meet of particles [mm],  
  * number of particles.
	
Result (to stdout):  
  - time of collision (in seconds) or False otherwise  
	
Example of usage:  
  - python particles.py 1 3000 1 1.5 2 10 100 100 2  
Result (undeterministic):  
  - 2935.0
	
Example 2:  
  - python particles.py 1 3000 1 1.5 2 10 100 100 4 -v

Example 3:  
  - python particles.py 1 3000 1 1.5 2 10 100 100 2 -t 10  
Result:  
  - 8 2

Other options:  
-h (--help)   
  - Print help.  
--meet-finder MEET_FINDER_NAME  
  - Choose meeting finding method ("octree" (default), "grid" or "bruteforce")
	
## Second version - GUI:
Usage:  
  - python gui.py

Main window:

![Main window](https://bytebucket.org/wojtek132/pracainz15/raw/da8a545f334b4c318ef9730f8a07f99899ce4299/vis/MainWindow2.png)
	
## Addon - program Statistics
Usage:  
  - python statistics.py
	
Than command-line interface will be shown.  
Example command:  
  - (Cmd) plotTestSelectedParameters 1 300 2 2 2 10 100 100 10 grid  
  - Tested parameter name:rMeet  
  - Tested parameter range (start, afterEnd, step):10 100 10  
  - Tested series name:particlesNumber  
  - Tested series range (start, afterEnd, step):2 4 1  
  - Number of repetition:100