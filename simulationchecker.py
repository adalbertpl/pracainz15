import cmd
import numpy as np
import os
import traceback
from os import read

from meetfinder import BruteforceMeetFinder, OctreeMeetFinder, GridMeetFinder
from particles import UserSimRunner, ValueVectorGenerator, SimConfig, FlatSimParams


def checkMeetFinders(line):
    stepTime, simTime, rangeX, rangeY, rangeZ, minSpeed, maxSpeed, rMeet, particlesNumber = map(float, line.split())
    particlesNumber = int(particlesNumber)
    times = 30
    params = FlatSimParams(stepTime, simTime, rangeX, rangeY, rangeZ, minSpeed, maxSpeed, rMeet, particlesNumber)
    pos = np.array([0, 0, 0])
    size = np.array([rangeX, rangeY, rangeZ])
    simConfig = SimConfig(False)

    print "Choose two meetfinders:"
    firstMeetFinder, secondMeetFinder = raw_input().split()

    def createMeetFinder(mfName):
        if mfName=="bruteforce":
            return BruteforceMeetFinder()
        elif mfName=="octree":
            return OctreeMeetFinder(pos, size)
        elif mfName=="grid":
            return GridMeetFinder(rMeet)
        else:
            raise Exception("Wrong meetfinder name")

    for i in range(1, times):
        print i,

        try:
            meetFinder1 = createMeetFinder(firstMeetFinder)
            simRunner = UserSimRunner(params, 1, simConfig, advancedResults=True, progressbar=False, printResults=False)
            simRunner.setMeetFinder(meetFinder1)
            result = simRunner.run()[0]

            print "{}".format(result["result"]),

            meetFinder2 = createMeetFinder(secondMeetFinder)
            #meetfinder2 = GridMeetFinder(rMeet)
            simRunner = UserSimRunner(params, 1, simConfig, startPosition=result["startPosition"], progressbar=False, printResults=False)
            simRunner.setMeetFinder(meetFinder2)
            simRunner.setGenerator(ValueVectorGenerator(result["generatorResults"]))
            result2 = simRunner.run()[0]

            if result["result"]!=result2["result"]:
                print " {} - WRONG!".format(result["result"], result2["result"])
            else:
                print " {}".format(result["result"], result2["result"])
                pass

        except Exception as e:
            print traceback.format_exc(e)

    print "\n"


class TestHelperCmd(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)

    def do_checkMeetFinders(self, line):
        try:
            checkMeetFinders(line)
        except Exception as e:
            print(str(traceback.format_exc()))


if __name__ == "__main__":

    cmd1 = TestHelperCmd()
    #main program loop
    cmd1.cmdloop()

    #import doctest
    #doctest.testmod()