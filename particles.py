#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

#sys.path.append(os.path.expanduser('./utils-local2'))

#Library will be first found on list?
import threading
from matplotlib import animation
from mpl_toolkits.mplot3d import axes3d

import sqlite3

if os.environ.get('USE_MY_UTILS_DEV_VERSION')=="1":
    sys.path.append(os.path.expanduser('~/AGH/Python/utils'))
    pass
elif os.environ.get('USE_MY_UTILS_DEV_VERSION')=="2":
    print "for this value of USE_MY_UTILS_DEV_VERSION program exit"
    raise Exception("Wrong USE_MY_UTILS_DEV_VERSION")
else:
    sys.path.append(os.path.expanduser('./utils-copy'))
    sys.path.append(os.path.expanduser('./utils'))
    #sys.path.append(os.path.expanduser('./utils-local'))
    pass

from collections import namedtuple
from myprogressbar import EmptyProgressBar, TerminalProgressBar, TkProgressBar


#from sympy import Plane, Point3D

from itertools import *

#import ProgressBar

import time
import json
import datetime

from mainutils import *
from geomutils import *
from randomw import *
from meetfinder import BruteforceMeetFinder, OctreeMeetFinder, MeetFinderFactory, GridMeetFinder, CythonGridMeetFinder
from meetfinder import Particle
#from octree import ParticlesOctree, Particle
from prism import *
#TODO: remove dependency from plyfile
#TODO: future warning (comparision to None) on windows (geom3d:207 - Sympy?)

#Program works in Windows (ParticlesCmd, ParticlesGUI). Works with Anaconda.
#Prerequisities:
#Installed Anaconda (2.5.0 - 64bit)
#conda install progressbar
#pip install pycontracts
#pip install plyfile

import sympy    #Version >=0.7.6.1

from sympy import *
#from sympy.physics.mechanics import ReferenceFrame, Vector, dot
#from sympy.physics.mechanics import dot

# class Point:
#   def __init__(self, x, y, z):
#       self.x = x
#       self.y = y
#       self.z = z

#def randomOnSphere():

#plane = sympy.Plane(sympy.Point3D(1,1,1), sympy.Point3D(1,1,1))

#test()

# logfile = open("particles.log", "w+")
#
# logging.basicConfig(stream=logfile, level=logging.DEBUG)
# log = logging.getLogger('Particles')
# log.setLevel(logging.DEBUG)
# log.info('Particles module')

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

log = initFileLogger("particles")

def test():
    plane = sympy.Plane((0,0,0), (1,1,1))

    segment = Segment3D((1,1,1), (2,2,2))

    print(segment)
    print(segment.midpoint)

    c = segment.intersection(plane)

    print (segment.contains(c[0]))

    print(plane)

    print (plane.angle_between(plane.perpendicular_line((2,2,2))))
    

def toNumpy(obj):
    if isinstance(obj, Point3D):
        return point3DToNumpy(obj)
    else:
        return tupleToNumpy(obj)


def point3DToNumpy(point3d):
    #symToNumpy
    #point3DtoNumpy
    return np.array([point3d.x, point3d.y, point3d.z]).astype(np.float64)

def tupleToNumpy(tuple):
    return np.array(tuple).astype(np.float64)


def sympyPlaneToTuple(plane):
    """ Return pair of numpy arrays from sympy plane.
        :returns: pair of arbitrary plane point and plane normal vector (normalized)
    """
    nv = toNumpy(plane.normal_vector)
    nvNormalized = normalize(nv)

    planePoint = toNumpy(plane.p1)
    return (planePoint, nvNormalized)


def crdList(dim):
    """ Return list (of size dim) of empty lists.
        It will contain transposed list of points (as lists of coordinates)
        >>> crdList(3)
        [[], [], []]
    """
    l = []
    [l.append([]) for i in range(dim)]
    return l


class VectorGenerator(object):
    def generate(self, number):
        #Generate method should remember history in self.history
        # if remember parameter set in constructor
        pass

    def getHistory(self):
        if self.remember==False:
            raise Exception("Remember parameter not set.")
        try:
            history = np.concatenate(self.history)
        except ValueError as e:
            log.info(e)
            #If history is empty
            history = np.array([])
        return history

class ValueVectorGenerator(VectorGenerator):
   def __init__(self, values):
       """ Initialize ValueVectorGenerator.
       This class returns pregenerated vectors.
       Vectors will be returned from first to last

       :param values: 2D numpy arrays of vectors
       """
       self.values = values
       self.last = 0
       #Number of values must be enough, because in generate it is not checked
       # and exception may be thrown

   def generate(self, number):
       #What to do if there is no remaining vectors?
       if self.last + number > len(self.values):
           print "Too few values of generator"
           print "number: {}, values: {}, len(values): {}".format(number, self.values, len(self.values))
       result = self.values[self.last : self.last+number]
       self.last += number
       return result


class SphericalBruteforceVectorGenerator(VectorGenerator):

    def __init__(self, minValue, maxValue, remember=False):
        """ Initialize object. """
        self.minValue = minValue
        self.maxValue = maxValue

        self.remember = remember
        self.history = []

        #TODO?: :param valueGenerator: generator class for generating value of vector

    @staticmethod
    def _sphereMonteCarlo():
        """ Generate uniformly distributed random point on sphere (2-sphere).
            :returns: one point (vector)

        >>> np.allclose(np.linalg.norm(PointGenerator.sphere()), 1)
        True
        """
        #vec = PointGenerator.cube(3)
        vec = [(2*random.random()-1) for i in range(3)]
        dist = np.linalg.norm(vec)

        #For uniform distribution all layers which not contain full spheres must be decline
        while (dist > 1):
            vec = [(2*random.random()-1) for i in range(3)]
            dist = np.linalg.norm(vec)

        vec = normalize(vec)

        return vec

    def generate(self, number):
        #Generate number of vectors as np.array of vectors
        #Number must be 1

        result = np.zeros((number, 3))

        for i in range(number):
            randDirection = SphericalBruteforceVectorGenerator._sphereMonteCarlo()
            #print ("randDirection", randDirection)
            #TODO: check performance of method calling, list creation, field access and list element access
            result[i] = randDirection * np.random.uniform(self.minValue, self.maxValue)

        if self.remember:
            self.history.append(result)

        return result


#Import module compiled from Cython
import meetFinderCython

class SphericalCythonVectorGenerator(SphericalBruteforceVectorGenerator):
    def generate(self, number):
        result = meetFinderCython.generateRandomVectors(self.minValue, self.maxValue, number)
        if self.remember:
            self.history.append(result)

        return result


class ParticlesJsonDecoder(object):
    @staticmethod
    def toNumpy(jsonObj):
        return np.array(jsonObj)


class SimRunner(object):
    def __init__(self):
        self.meetCount = 0
        self.results = []
        self.generator = None

    #def runMultipleTimes(self)
    @staticmethod
    def changeParamsFormat(args):
        #Should simulation parameters be numbers or could be complex types - list etc?
        return [args.stepTime,
            args.simTime,
            [args.rangeX, args.rangeY, args.rangeZ],
            args.minSpeed,
            args.maxSpeed,
            args.rMeet,
            args.particlesNumber]

    @staticmethod
    def flatParamsFormat(args):
        return FlatSimParams(args[0], args[1], args[2][0], args[2][1], args[2][2],
                args[3], args[4], args[5], args[6])

    def setMeetFinder(self, meetFinder):
        self.meetFinder = meetFinder

    def setGenerator(self, generator):
        self.generator = generator

    def setSimVisualization(self, vis):
        self.simVisualization = vis

    def getSimVisualization(self):
        return self.simVisualization

    #def setParamsGen(self)
    #def getParams(self)

    def afterSimulation(self, result):
        pass

    def runMultipleTimes(self, params, times, progressbar=False, pbPlace="terminal",
                         startPosition="random", dbSave=False):
        """ Run simulation multiple times. Count number of meetings.
            One progress bar for all simulations.
            Progress bar parameters - see BrownianMotion class.
            self.conf.withPlots - if set, visualization should be show?
            :param times: run simulation "times" times.
            :returns: count of particles meetings
        """
        if progressbar:
            if pbPlace=="terminal":
                pbar = TerminalProgressBar(times)
            else:
                #TODO: maybe create this class in gui?
                pbar = TkProgressBar(pbPlace, times)

        else:
            pbar = EmptyProgressBar()

        meetCount = 0

        for i in xrange(times):
            #Multiple run can be with visualization too
            self.runOneSimulation(params, progressbar=False, startPosition=startPosition, dbSave=dbSave)
            pbar.update(i)

        pbar.finish()

        return self.meetCount

    @staticmethod
    def databaseExists():
        con = sqlite3.connect("simulationResults.db")
        cur = con.cursor()
        results = cur.execute("select name from sqlite_master where type='table' and name='simResults'")
        if results.fetchone()!=None:
            return True
        else:
            return False

        #con.close()

    @staticmethod
    def createDatabase():
        if SimRunner.databaseExists():
            return 0
        else:
            con = sqlite3.connect("simulationResults.db")
            cur = con.cursor()
            cur.execute("create table simResults(simulationDate date, computationTime real, stepTime real, simTime real, \
                        rangeX real, rangeY real, rangeZ real, minSpeed real, maxSpeed real, rMeet real, particlesNumber int, \
                        meetfinder text, result real)")
            con.commit()
            return 1


    def runOneSimulation(self, params, progressbar=True, pbPlace=None, advancedResults=False, startPosition="random", dbSave=False):
        if progressbar==True and pbPlace==None:
            raise Exception("Place of progressbar not set.")

        #Should progressbar class be inserted from outside?
        #Should we use self.dbSave or function argument in this method?
        self.meetFinder.clear() #Restart meetFinder, remove all particles
        brown = BrownianMotion(*(self.changeParamsFormat(params)), progressbar=progressbar, pbPlace=pbPlace,
                               meetFinder = self.meetFinder, advancedResults=advancedResults, generator=self.generator)
        brown.setBorderCondition(ReflectCuboidBorderCondition([params.rangeX, params.rangeY, params.rangeZ]))
        if startPosition!="random":
            brown.setStartPosition(startPosition)

        # --- Run simulation ---
        result = brown.simulate()
        result["params"] = params #TODO: set params inside BrownianMotion
        self.results.append(result)

        #Special string = "False"
        if result["result"]!="False":
            self.meetCount += 1

        if dbSave==True:
            #Save result in database
            #It seems to work
            if not SimRunner.databaseExists():
                print "Warning. Database didn't exist. Database created."
                SimRunner.createDatabase()

            con = sqlite3.connect("simulationResults.db")
            cur = con.cursor()
            #TODO: computer name/hostname, computation time, id
            now = datetime.datetime.now()
            mfName =  MeetFinderFactory.getName(self.meetFinder)

            print params

            #0 should be save to database when time is longer that simulation time
            cur.execute("insert into simResults(simulationDate, computationTime, stepTime, simTime, \
                        rangeX, rangeY, rangeZ, minSpeed, maxSpeed, rMeet, particlesNumber,  \
                        meetfinder, result) values (?, ?, ?, ?, \
                        ?, ?, ?, ?, ?, ?, ?, \
                        ?, ?);", (now, None) + params + (mfName, result["result"]))

            con.commit()

        return result

    def afterAllSimulations(self, results):
        pass


# class DatabaseSimRunner(SimRunner):
#     def __init__(self, paramGen):
#         super(DatabaseSimRunner, self).__init__()
#         pass
#
#     def afterSimulation(self, results):
#         pass


class UserSimRunner(SimRunner):
    def __init__(self, simParams, times, conf, pbPlace="terminal",
                 advancedResults=False, startPosition="random", dbSave=False, animation=False, progressbar=True, printResults=True):
        """ Initialize SimRunner.
        :param simParams: parameters of simulation - numbers that we can save in database
        :param times: number of repetition of simulation
        :param startPosition: "random" or np.array of particles positions
        :return:
        """
        #TODO: should "times" be part of SimRunner of of BrownianMotion?
        super(UserSimRunner, self).__init__()
        self.params = simParams
        self.times = times
        self.conf = conf
        self.pbPlace = pbPlace
        self.animation = animation
        self.dbSave = dbSave
        self.progressbar = progressbar

        self.advancedResults = advancedResults
        self.startPosition = startPosition
        self.printResults = printResults

    def run(self):
        #Should we save this runs too in database?
        #TODO: maybe other parameter for returning counts?
        #pdb.set_trace()
        #if len(args.times)>1:
        #    raise Exception("times parameter used multiple times")

        times = self.times
        #TODO: this function may be part of BrownianMotion?
        #What if we will want to use threads?

        #Two different cases - one time or multiple times
        if times==1:
            result = self.runOneSimulation(self.params, progressbar=self.progressbar, pbPlace=self.pbPlace,
                                           advancedResults=self.advancedResults, startPosition=self.startPosition, dbSave=self.dbSave)
            # --- Result of program: ---
            if self.printResults:
                print result["result"]

        else:
            if self.advancedResults!=False:
                print "Advanced results cannot be set for multisimulation"
            self.runMultipleTimes(self.params, times, progressbar=self.progressbar, pbPlace=self.pbPlace,
                                  startPosition=self.startPosition, dbSave=self.dbSave)
            meetCount = self.meetCount
            # --- Result of program: ---
            if self.printResults:
                print "{0} {1}".format(meetCount, times-meetCount)

        self.afterAllSimulations(self.results)

        return self.results

    def afterAllSimulations(self, results):
        if self.conf.withPlots:
            vis = self.getSimVisualization()
            vis.setSimData(results[0]) #TODO: plot all results
            vis.visualize(doPlotAnimation=self.animation)
            ##FIXME: animaation parameter
            #FIXME: some problems
            #self.plotStat()


FlatSimParams = namedtuple("FlatSimParams", ["stepTime", "simTime",
                                             "rangeX", "rangeY", "rangeZ",
                                             "minSpeed", "maxSpeed", "rMeet",
                                             "particlesNumber"], verbose=False)

# class FlatSimParams(FlatSimParamsNamedtuple):
#     pass

SimConfig = namedtuple("SimConfig", ["withPlots"])

class MayaviThread(threading.Thread):
    def __init__(self, visType, conf):
        super(MayaviThread, self).__init__()
        self.visType = visType
        self.simData = conf["simData"]

    def run(self):
        if self.visType=="animation":
            self.animatedTrackHistoryMayavi()


    def animatedTrackHistoryMayavi(self):
        from tvtk.tools.visual import sphere
        print "animatedTrackHistoryMayavi"
        #positions = map(lambda x: x[0], self.simData["track"])
        tracks = self.simData["track"]
        steps = len(tracks)
        balls = []

        for track in tracks:
            ball = sphere(radius=0.01, color=(0, 0, 1))
            balls.append(ball)

        for i in range(steps):
            for j in range(len(tracks)):
                ball = balls[j]
                ball.pos = np.array(tracks)[j, 0:3, i]
            pass

        print "Not implemented"
        pass


class ParticlesSimVis(object):

    def __init__(self, visual, oneFigure=False):
        #self.simData = simData
        self.visual = visual
        self.ax = None
        self.oneFigure = oneFigure
        #self.doPlotAnimation = animation
        #TODO: what with real-time visualization?
        #Maybe simData.realtime and realtime=True?
        #Need some thinking

    def setSimData(self, simData):
        self.simData = simData

    def visualize(self, doPlotAnimation=False):
        """ Visualization of track of particles.
        """
        log.info("visualize")

        self.plot3d()
        #self.plot2dProjection()
        #print "visualize"
        #print "doPlotAnimation: {}".format(doPlotAnimation)
        if doPlotAnimation:
            self.animatedTrackHistory()

    @listify
    def startLines(self, ax, simData):
        for line in simData.lines:
            #simData["track"]?
            axLine = ax.plot(line[0][0:1], line[1][0:1], line[2][0:1])[0]
            yield axLine

    def configureAxes(self, ax):
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_title("Animated particles track")

    def animatedTrackHistoryMatplotlib(self):
        """ Plot animated particle movement """

        def update_lines(num, lines, linePositions, other):

            for line, linePosition in zip(lines, linePositions):
                line.set_data(linePosition[0:2, 0:num])
                #line.set_3d_properties(linePosition[2, 0:num])
            #return lines
            return lines

        fig = plt.figure()
        animTrackAx = axes3d.Axes3D(fig)
        self.configureAxes(animTrackAx)
        lines = self.startLines(animTrackAx, self.simData)

        #maybe change blit?
        line_ani = animation.FuncAnimation(fig, update_lines, 10, fargs=(lines, np.array(self.simData["track"]), {}), interval=30, blit=True)
        plt.show(block=False)

    def animatedTrackHistory(self):
        #self.animatedTrackHistoryMatplotlib()
        MayaviThread("animation", {"simData": self.simData}).start()

    def plot2dProjection(self):
        #fig = plt.figure()
        #plt.scatter(self.points[0], self.points[1], s=100, cmap=plt.cm.cool, edgecolors='None', alpha=0.75)
        plt.plot(self.simData.line[0][0], self.simData.line[0][1])
        plt.plot(self.simData.line[1][0], self.simData.line[1][1])
        #plt.plot(self.points[0], self.points[1])
        #plt.colorbar()
        plt.show()

    def plotBall(self, x, y, z, plotter, color='y', size=200):
        plotter.scatter(x, y, z, c=color, s=size)

    def plot3d(self):
        """ Plot track of particles from ended simulation.

        :return:
        """
        #Maybe one figure parameter is not needed.
        oneFigure = self.oneFigure
        #def randrange(n, vmin, vmax):
        #    return (vmax-vmin)*np.random.rand(n) + vmin
        #TODO: 300000 moves - plot time is very long (60s?)

        nonTkVisual = ["newWindow", "newWindowBlocked"]

        if (not self.oneFigure) or self.ax==None or (self.visual not in nonTkVisual):
            fig = plt.figure()
            self.ax = fig.add_subplot(111, projection='3d')
            self.ax.set_xlabel('X')
            self.ax.set_ylabel('Y')
            self.ax.set_zlabel('Z')

            if self.visual in nonTkVisual:
                plt.show(block=False) #False, when run from GUI

        ax = self.ax

        #Length of x line of first particle
        n = len(self.simData["track"][0][0])

        #colors = cycle(['r', 'b'])
        colors = cycle(['r', 'b', 'g', 'y', 'm', '#771010', '#101077'])

        if self.visual in nonTkVisual:
            ax.clear()


        for i in xrange(self.simData["params"].particlesNumber):
            #TODO: why is grid showing on top of particles trace?
            #TODO: maybe show part of lines for big simulations?
            #Maybe only last 100 history points?
            line = self.simData["track"][i]
            ax.plot(*line, c=next(colors)) #it works
            #start position
            #self.plotBall(line[0][0], line[1][0], line[2][0], ax, color='g', size=200)
            #end position
            self.plotBall(line[0][n-1], line[1][n-1], line[2][n-1], ax, color='y', size=200)

        #ax.plot(xs, ys, zs, c='b')
        #ax.plot(*self.line2, c='r') #it works
        #ax.scatter(xs[n-1], ys[n-1], zs[n-1], c='y', s=200)

        #visual here should not be None
        #TODO: maybe check if started from terminal?
        #Rarely there are problems with plots.
        if self.visual == "newWindow":
            #Plot in window (interactive plot)
            plt.draw()
            #    plt.draw()
            #else:
            pass

        elif self.visual == "newWindowBlocked":
            #TODO: does this work yet?
            plt.show(block=True)

        else:
            #Plot in Tkinter class
            self.visual.plot(fig)

            #tk.mainloop()

    def plotStat(self):
        """ Plot some data about particles.
        """
        if self.particlesNumber==2:
            #Distance plot now work only for two particles
            self.distanceCalculate()
            plt.plot(range(len(self.dstList)), self.dstList) #Plot distance for each step
            plt.show()

class BorderCondition:
    pass

#TODO: what to do with meetFinder
# with other border conditions than ReflectCuboidBorderCondition?
class ReflectCuboidBorderCondition(BorderCondition):
    def __init__(self, cuboidRangeMax):
        self.cuboid = Cuboid(cuboidRangeMax)
        self.planes = list(self.cuboid.getPlanes())

    def crossed(self, plane, oldPosition, newPosition):
        #TODO: Move to method crossedPlane
        particle = oldPosition
        move = newPosition - oldPosition
        planePoint, nv = plane.planePoint, plane.nvNormalized
        particleFromPlane = particle - planePoint

        particleSide = np.sign(np.dot(particleFromPlane, nv))
        moveSide = np.sign(np.dot(particleFromPlane + move, nv))
        result = not (particleSide == moveSide)
        return result

    def crossedMultiple(self, plane, oldPositions, newPositions):
        #rowCount = np.shape(oldPositions)[0]
        #results = np.zeros(rowCount)
        #for i in range(rowCount):
        #    results[i] = self.crossed(oldPositions[i], newPositions[i])

        particles = oldPositions
        moves = newPositions - oldPositions
        planePoint, nv = plane.planePoint, plane.nvNormalized
        particlesFromPlane = particles - planePoint

        particlesSide = np.sign(np.dot(particlesFromPlane, nv))
        movesSide = np.sign(np.dot(particlesFromPlane + moves, nv))
        results = (particlesSide != movesSide)
        #results = results.all(axis=1)

        return results

    def correctParticle(self, plane, particle):
        #Move to method reflectFromPlane
        planePoint, nvNormalized = plane.planePoint, plane.nvNormalized
        tmpProj = nvNormalized * np.dot(nvNormalized, particle - planePoint)
        reflected = particle - 2*tmpProj
        return reflected


class ParticlesJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            print "JsonEncoder - numpy"
            return obj.tolist()
        if isinstance(obj, tuple): #and hasattr(obj, "_fields"):
            #return obj.jsonencode()
            print "JsonEncoder - FlatSimParams"
            return obj._asdict()
        return json.JSONEncoder.default(self, obj)


class SimData:
    pass


class BrownianMotion:
    """ Class for simulating Brownian motion.
        There are two particles with random start position.
        Simulation ends when they collide or when time ends.

    >>> brown = BrownianMotion(1, 100, [5, 6, 7], 10, 100, 1)
    >>> particle = PointGenerator.cuboid(brown.rangeMax)
    >>> p2 = brown.brownMove(particle)
    >>> dist = np.linalg.norm(p2 - particle)
    >>> (dist >= 10.0/1000) & (dist <= 100.0/1000)
    True
    >>>
    #Too much time for this example:
    #>>> brown.simulate() #False with high probability
    #False
    #>>> brown.outsideArea(np.array[4, 5, 6])
    #None
    #>>> result = brown.outsideArea(np.array[6, 0, 0])
    #>>> isinstance(result, Plane)
    #True
    """

    def __init__(self, stepTime, simTime, rangeMax, minSpeed, maxSpeed, rMeet, particlesNumber = 2,
                 visual=None, progressbar=True, pbPlace="terminal", border=None, meetFinder=BruteforceMeetFinder(), generator=None,
                 advancedResults=False):
        """ Initialize BrownianMotion parameters.
            Particles can move only in boundaries of [0,0,0] rangeMax.
            Add gui parameter if you want to show plots in gui (noninteractively)

            :param stepTime: in seconds (may be real)
            :param time: in seconds
            :param rangeMax: list or np.array (numbers in meters)
            :param minSpeed: in mm/s
            :param maxSpeed: in mm/s
            :param rMeet: in milimeters

            :param visual: None, "newWindow" or class (from Tkinter) to insert plots
            :param progressbar: Show one simulation progress bar in terminal
        """
        #Input parameters:
        #Use meters and seconds
        self.stepTime = float(stepTime)
        self.simTime = float(simTime)
        self.rangeMax = np.array(rangeMax)
        self.minSpeed = float(minSpeed) / 1000 #units change
        self.maxSpeed = float(maxSpeed) / 1000 #units change
        self.rMeet = float(rMeet) / 1000 #units change
        self.maxStep = float(self.simTime)/self.stepTime

        (self.minMove, self.maxMove) = (self.minSpeed * self.stepTime, self.maxSpeed * self.stepTime)

        self.params = FlatSimParams(self.stepTime, self.simTime, self.rangeMax[0], self.rangeMax[1], self.rangeMax[2], self.minSpeed, self.maxSpeed, self.rMeet, particlesNumber)

        self.cuboid = Cuboid(rangeMax)
        
        #self.planes = map(sympyPlaneToTuple, self.cuboid.getPlanes())
        self.planes = list(self.cuboid.getPlanes())

        self.particlesNumber = particlesNumber
        #self.octree = ParticlesOctree([0, 0, 0], rangeMax)
        #TODO: Assign meetFinder or create here MeetFinder?
        #If create here - we will need parameters or configured MeetFinder factory,
        #Maybe write this factory?
        self.meetFinder = meetFinder
        self.meetFinder.setMeetParams(self.rMeet)

        self.visual=visual
        self.progressbar = progressbar
        self.pbPlace = pbPlace
        self.advancedResults = advancedResults

        #Data for visualization
        #Each line represent one point
        self.lines = [crdList(3) for i in xrange(particlesNumber)]

        #TODO: test if range>maxSpeed

        if generator==None:
            #self.vectorGenerator = SphericalBruteforceVectorGenerator(self.minMove, self.maxMove, remember=self.advancedResults)
            self.vectorGenerator = SphericalCythonVectorGenerator(self.minMove, self.maxMove, remember=self.advancedResults)
        else:
            self.vectorGenerator = generator

        if border==None:
            border = ReflectCuboidBorderCondition(self.rangeMax)

        self.border = border

        self.simData = {} #SimData() #Information abouts simulation (parameters, track etc.)

        #Maybe change name of this method?
        #Maybe this should be here not in method
        self._initSimulation()

    def setBorderCondition(self, borderCondition):
        self.border = borderCondition

    def setVectorGenerator(self, vectorGenerator):
        self.vectorGenerator = vectorGenerator

    #Method deprecated
    @staticmethod
    def reflectFromPlane(plane, particle):
        """ Reflect particle actual position from plane.
            Used for bouncing particle from boundaries.
            :param plane: Reflection plane 
                as pair of (planePoint, normalVectorNormalized)
            :type plane: tuple of nd.arrays
            :param particle: Particle to reflect
            :results: Particle after reflection
        >>> import prism
        >>> plane = prism.Plane((0, 0, 0), (0, 0, 1))
        >>> np.allclose(BrownianMotion.reflectFromPlane(plane, (0, 0, 1)), (0, 0, -1))
        True
        >>> np.allclose(BrownianMotion.reflectFromPlane(plane, (0.5, 0.5, 1)), (0.5, 0.5, -1))
        True
        """
        ##Pomysl - moze do doctest umieszczac dodatkowy kod inicjalizujacy poza doctest?
        ##Ale wtedy moze lepiej unittest?
        #print plane.normal_vector
        #nv = toNumpy(plane.normal_vector)
        #print nv
        #nvNormalized = normalize(nv)
        #planePoint = toNumpy(plane.p1)
        planePoint, nvNormalized = plane.planePoint, plane.nvNormalized

        tmpProj = nvNormalized * np.dot(nvNormalized, particle - planePoint)
        
        reflected = particle - 2*tmpProj
        #print reflected
        return reflected







    #Maybe:
    #Class GeometryHelper - not needed
    #Change some methods to private

    #Method deprecated
    @staticmethod
    #@signature(Plane, Point3D, Point3D) - is this not working with doctest?
    def crossedPlane(plane, particles, moves):
        """ Return if plane is crossed by particle in this move.
            :param plane: plane as tuple of arbitrary plane point and normal vector
            :param particle: particle before move
            :param move: vector of move
            :results: True if crossed, False otherwise
        >>> plane = (np.array([0, 0, 0]), np.array([0, 0, 1]))
        >>> BrownianMotion.crossedPlane(plane, (0, 0, 0.1), (0, 0, -0.2))
        True
        >>> BrownianMotion.crossedPlane(plane, (0, 0, 0.1), (0, 0, 0.2))
        False
        """
        #this two toNumpy calls used 74,32% of processor time
        #19.11.2015 - program run 99,5s (-t 10 - ten times) - python particles.py 2 5000 2 2 2 10 100 150 -t 10
        #nv = toNumpy(plane.normal_vector)
        #planePoint = toNumpy(plane.p1)
        planePoint, nv = plane.planePoint, plane.nvNormalized

        particleFromPlane = particles - planePoint
        #print particleFromPlane

        #Scalar product of normal vector and another vector in plane coordinates
        #particleSide = np.sign(particleFromPlane.dot(nv))
        particleSide = np.sign(np.dot(particleFromPlane, nv))
        moveSide = np.sign(np.dot(particleFromPlane + moves, nv))

        result = not (particleSide == moveSide)

        return result #Return np.array of True/False

    def forceMove(self, particles):

        newPositions = particles.tolist()

        for i in len(newPositions):
            forcePositions, forceMasses = self.meetfinder.findForceSources(particles[i], coefficient)

            for j in len(forcePositions):
                newPosition[i] = newPosition[i] + \
                                 self.computeImpulse(particles[i], self.particlesMasses[i], self.forcePositions[i], self.forceMasses[i])

    def brownMove(self, p, particle=None):
        """ Random move particle to new position and return moved particle.
            Particle will bounce from range boundaries.
            Method should work for array of particles
            Private method.
            :param p: particle (as numpy.array); not changed
            :param particle: particle as Particle
            :returns: moved particle (as numpy.array)
        """
        #(minMove, maxMove) = (self.minSpeed * self.stepTime, self.maxSpeed * self.stepTime)

        rowsCount = np.shape(p)[0]
        randMoveVectors = self.vectorGenerator.generate(rowsCount)

        newPositions = p + randMoveVectors
        #print "Rand: {}".format(randMoveVectors)
        #print newPositions

        # for plane in self.planes:
        #     if self.border.crossed(plane, p, newPosition):
        #     #if BrownianMotion.crossedPlane(plane, p, randMoveVector):
        #         #TODO: order of plane reflections
        #         #TODO: check in tests if particle could move out of cuboid when in corner
        #         #newPosition = BrownianMotion.reflectFromPlane(plane, newPosition)
        #         newPosition = self.border.correctParticle(plane, newPosition)

        #for plane in self.planes:
        #    for i in range(rowsCount):
        #        if self.border.crossed(plane, p[i], newPositions[i]):
        #            newPositions[i] = self.border.correctParticle(plane, newPositions[i])

        for plane in self.planes:
            crossed = self.border.crossedMultiple(plane, p, newPositions)
            #print crossed
            #Get indices of elements which crosses border
            for i in np.where(crossed==True)[0]:
                newPositions[i] = self.border.correctParticle(plane, newPositions[i])

        #Test if no error
        #FIXME: why AssertionError?
        #oldPosition = p
        #for plane in self.cuboid.getPlanes():
        #    assert(not BrownianMotion.crossedPlane(plane, oldPosition, newPosition)) #Check if on the same side

        #self.octree.getBox(particle)
        #Move particle to new position and inform meetFinder
        self.meetFinder.moveParticles(p, newPositions)
        #particle.position = newPosition
        self.particles = newPositions

        return newPositions

    @staticmethod
    def exampleParams():
        """ Return example input parameters. 
            May be helpful for running from another module.
            :returns: Tuple of parameters which can be used into constructor.
        """
        return (1, 3000, [1, 1.5, 2], 10, 100, 100, 2)

    def outsideArea(self, particle):
        """ Check if particle is outside area of computation.
            :returns: None if inside, Plane of 
        """
        pass

    #def step(self):

    #TODO: rename
    def addPointToLine(self, point, line):
        """ Add point coordinates to history track of each point.
        """
        #for i in range(3):
        #    print (i, line[i], point[i])
        #pdb.set_trace()
        [line[i].append(point[i]) for i in range(3)]

        #line[1].append(point[1])
        return

    def distanceCalculate(self):
        """ Return array of distances between two particles in steps.
        """
        #TODO: now it works good only for two particles,
        #   maybe should return distance between collided points
        pt1Track = np.array([self.line[0][0], self.line[0][1]]).T
        pt2Track = np.array([self.line[1][0], self.line[1][1]]).T
        #pairs = zip(pt1Track, pt2Track)
        #self.dstList = map(lambda (x, y): norm(x-y), pairs)
        #self.dstList = np.linalg.norm(pt1Track-pt2Track, axis=-1)
        self.dstList = arrayNorm(pt1Track-pt2Track)

    def endComputation(self):
        simData = self.simData
        simData["result"] = self.result
        simData["track"] = self.lines #track

        simData["params"] = self.params

        #TODO: rename
        if self.advancedResults:
            simData["startPosition"] = self.startPosition
            simData["generatorResults"] = self.vectorGenerator.getHistory()
        #simData.historyLines
        #simData.params - assigned from outside after returning simData?
        self.simData = simData

        return simData

    def getSimData(self):
        return self.simData

    #TODO: Unit tests

    # def findMeeting(self, particles):
    #     """ Find if there is any collision between particles.
    #         :param particles: list of particles positions
    #         :returns: True if there is any collision, False otherwise
    #     """

    def addStateToHistory(self, particles):
        for i in xrange(self.particlesNumber):
            p = particles[i]
            self.addPointToLine(particles[i], self.lines[i])        

    def _initSimulation(self):
        #self.particles = [Particle(PointGenerator.cuboid(self.rangeMax)) for i in xrange(self.particlesNumber)]
        self.setStartPosition(np.array([(PointGenerator.cuboid(self.rangeMax)) for i in xrange(self.particlesNumber)]))

    @property
    def particleObjects(self):
        return (Particle(self.particles, i) for i in xrange(self.particlesNumber))

    def setStartPosition(self, particles):
        """ Set start particles and its positions (change start position from random to fixed).
            Particles count must be consistent with particlesNumber.
            :param particles: list of particles
        """
        #Method may help debugging.
        self.startPosition = particles
        self.particles = particles
        if len(self.particles)!=self.particlesNumber:
            raise Exception("Wrong count of particles.")


    #BruteforceMeetFinder
    #5p,300st,2m, 10-100v(mm/s), 100r
    #38:62 (meet vs not-meet)
    #313:689

    def simulate(self):
        """ Run simulation until it ends (collision or reach maxSteps).
            After end it saves steps count in self.simulationSteps.

            :returns: time of collision (in seconds) or "False" string otherwise
        """
        #Particles position here should be set
        n = self.particlesNumber

        step = 0

        #TODO: default EmptyProgressBar
        #   method setProgressBar that will set object of ProgressBar interface
        #Progressbar factory?

        #Progress bar start
        if self.progressbar:
            if self.pbPlace=="terminal":
                pbar = TerminalProgressBar(self.maxStep)
            else:
                #TODO: maybe create this class in gui?
                pbar = TkProgressBar(self.pbPlace, self.maxStep)
        else:
            pbar = EmptyProgressBar()

        #particles - list of particles
        #random point in cuboid

        particles = self.particles

        self.meetFinder.initParticles(list(self.particleObjects))
        #self.meetFinder.initParticles(self.particles)

        #self.addStateToHistory(map(lambda x: x.position, particles))
        self.addStateToHistory(particles)

        #TODO: show in visualization only end positions and trace of meet particles where there are e.g. >100 particles.
        #TODO: octree visualization
        #TODO: Spradzic wyniki. Niekiedy wydaje mi sie, ze jest jakas zaleznosc pomiedzy kolejnymi wynikami

        self.meetFinder.newStep(step)

        while(step!=self.maxStep):
            #step count - count of states that were in simulation

            #FIXME: some cases in finding meetings by octree (when particle is near face of box) need to be done

            if self.meetFinder.foundMeeting():
                #TODO: maybe refactor?
                #Maybe next lines should be after simulation loop?
                #But meeting step must be correct
                log.info("Meeting")
                pbar.finish()
                #stepTime - time of one step as defined for simulation (not computation time)
                #TODO: when meeting is in step 0, counter is counting at as non-meeting
                self.result = self.meetFinder.getFirstMeetingStep() * self.stepTime
                #self.endComputation()
                break


            #step += 1
            #TODO: maybe change in place?
            #TODO: numpy?
            #particles = map(self.brownMove, particles)
            # for particle in particles:
            #     self.brownMove(particle.position, particle=particle)
            self.brownMove(particles)
            particles = self.particles #Maybe rafactor?
            #p1 = self.brownMove(p1)


            #Add actual positions to history track
            self.addStateToHistory(particles)

            #if (step % 5000 == 0):
                #Visualize every 5 steps
            #    self.visualize()

            #TODO: check performance of progress bar, maybe time interrupt
            #progress bar
            #TODO: more preciser progressbar (when multiple particles)
            if (step%100==0) and (self.progressbar) :
            #     sys.stdout.write('Step: {0}\r'.format(step))
            #     sys.stdout.flush()
                pbar.update(step)

            step += 1

            #Where should be this method?
            self.meetFinder.newStep(step)


        #may be 0
        #self.simulationSteps = step + 1

        #print ("simulate - test1")


        if (step>=self.maxStep):
            log.info("Maximum number of steps")

            if self.progressbar:
                pbar.finish()

            if self.meetFinder.getFirstMeetingStep()!=None:
                #Check after maxStep if meeting was found
                self.result = self.meetFinder.getFirstMeetingStep() * self.stepTime
            else:
                #Return special string
                #We cannot return False because False==0
                self.result = "False"


        self.endComputation()
        
        return self.simData



#TODO: tests





def runParticles(inputArgs):
    """ Run brownian motion simulation of multiple particles until it ends (collision or reach maxSteps).
        Arguments are stepTime [s], simTime [s], rangeX [m], rangeY [m], rangeZ [m], 
            minSpeed [mm/s], maxSpeed [mm/s], rMeet [mm], particlesNumber.
        :returns: time of collision (in seconds) or False otherwise

        #TODO: not works now
        >>> runParticles([1, 10, 1, 1.5, 2, 10, 100, 100]) #Should return False with high probability (how high?)
        False
        >>> runParticles("1 2 1 1.5 2 10 100 100 -t 10") #with high probability (how high?)
        0 11
        >>> runParticles("1 2 1 1.5 2 10 100 100 -v") #with high probability (how high?); show visualization
        False
        >>> runParticles([1, 10, 1, 1.5, 2, 10, 100, 100, 3]) #3 particles; should return False with high probability (how high?)
        False
        >>> runParticles("-h") #doctest: +ELLIPSIS
        ... help ...
    """
    #Command line version of BrownianMotion

    params = BrownianMotion.exampleParams() #(1, 3000, [1, 1.5, 2], 10, 100, 100)


    parser = argparse.ArgumentParser(description='Particles program. Run without arguments for default parameters.')

    #TODO: subparser
    parser.add_argument('stepTime', type=float, help='stepTime [s]', default=params[0])
    parser.add_argument('simTime', type=float, help='simTime [s]', default=params[1])
    parser.add_argument('rangeX', type=float, help='rangeX [m]', default=params[2][0])
    parser.add_argument('rangeY', type=float,  help='rangeY [m]', default=params[2][1])
    parser.add_argument('rangeZ', type=float,  help='rangeZ [m]', default=params[2][2])
    parser.add_argument('minSpeed', type=float,  help='minSpeed [mm/s]', default=params[3])
    parser.add_argument('maxSpeed', type=float,  help='maxSpeed [mm/s]', default=params[4])
    parser.add_argument('rMeet', type=float,  help='rMeet [mm]', default=params[5])
    parser.add_argument('particlesNumber', type=int,  help='particlesNumber', default=params[5])

    #run in debug mode
    parser.add_argument('-v', dest='withPlots', action='store_const', const="newWindowBlocked", default=None, help='Debug mode (with visualization)')

    #run multiple times
    #FIXME?
    parser.add_argument('-t', dest='times', type=int, default=1, help='Run multiple times (will return only counts)')

    parser.add_argument('--meet-finder', dest='meetFinder', type=str, default="octree", help='Select another finding meeting algorithm. Possible values: octree, bruteforce, grid') #, grid

    #FIXME: not works yet
    #parser.add_argument('-m', dest='measurePerformance', action='store_const', const=True, default=False, help='Measure simulation performance. Other arguments are ignored')

    #if not hasattr(args, 'stepTime'):

    #BrownianMotionStat.particlesNumberTimePlot()
    #return

    if len(inputArgs)==0:
        #program was run without parameters (example)
        #brown = BrownianMotion(*params, visual="newWindowBlocked")
        args = UserSimRunner.flatParamsFormat(params)
        conf = SimConfig(withPlots=True)
        runner = UserSimRunner(args, 1, conf)
        runner.setSimVisualization(ParticlesSimVis("newWindowBlocked")) #Maybe change it?
        runner.setMeetFinder(OctreeMeetFinder(np.array([0,0,0]), np.array([args[2], args[3], args[4]])))
        runner.run()        #result = brown.simulate()
        #print result

    else:
        args = parser.parse_args(inputArgs)

        #print args.measurePerformance

        #Check if special options are set

        #FIXME: not works yet
        #if args.measurePerformance==True:
        #Ignore other arguments

        #    BrownianMotionStat.particlesNumberTimePlot()
        #    return

        if args.meetFinder=="octree":
            #Default option
            #TODO: octree meetfinder results sometimes seems different than bruteforce:
            #./particles.py 1 300 2 2 2 10 100 100 5 -t200 --meet-finder octree
            #195 5
            #./particles.py 1 300 2 2 2 10 100 100 5 -t200 --meet-finder bruteforce
            #73 127
            meetFinder = OctreeMeetFinder(np.array([0,0,0]), np.array([args.rangeX, args.rangeY, args.rangeZ]))
        elif args.meetFinder=="bruteforce":
            meetFinder = BruteforceMeetFinder()
        elif args.meetFinder=="grid":
            meetFinder = CythonGridMeetFinder(args.rMeet)
        elif args.meetFinder=="slowergrid":
            meetFinder = GridMeetFinder(args.rMeet)
        else:
            raise Exception("Wrong meetFinder option chosen.")

        # factory = MeetFinderFactory()

        #It seems to work (partially)
        runner = UserSimRunner(args, args.times, args, progressbar=args.withPlots) #Print progressbar only in debug mode
        runner.setSimVisualization(ParticlesSimVis("newWindowBlocked")) #Maybe change it?
        runner.setMeetFinder(meetFinder)
        runner.run()



    return 0


if __name__ == "__main__":
    #import doctest
    #doctest.testmod()

    #print sys.argv[1:]

    runParticles(sys.argv[1:])


    
    #input("Press Enter to continue...")
    
    
