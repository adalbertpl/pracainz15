# cython: profile=True

import numpy as np
cimport numpy as np
cimport cython
from cpython cimport array
import array

DTYPE = np.float
ctypedef np.float_t DTYPE_T

#https://groups.google.com/forum/#!topic/cython-users/jc-3UK2Ffoc
from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt
cdef double RAND_SCALE = 1.0 / RAND_MAX

cdef inline double nextRand(float a):
    return rand() * RAND_SCALE * a

#@cython.boundscheck(False)
def generateRandomVectors(float minValue, float maxValue, int number):
    cdef np.ndarray result = np.zeros([number, 3], dtype=DTYPE)
    cdef int i
    cdef float x, y, z, s, s2

    i = 0
    while i<(number):
        x = nextRand(2.0) - 1.0
        y = nextRand(2.0) - 1.0
        z = nextRand(2.0) - 1.0

        s = x*x+y*y+z*z
        if s <= 1.0:
            s2 = sqrt(s)
            value = minValue + nextRand(maxValue - minValue)

            x = x/s2 * value
            y = y/s2 * value
            z = z/s2 * value
            result[i, 0] = x
            result[i, 1] = y
            result[i, 2] = z
            i = i+1

    #print result
    return result

from itertools import combinations

cdef checkAllMeetings(particles, rMeet):
    """ Return True if any meeting between particles was found.
        :param particles: np.array of particles positions
    """
    #Maybe class?
    #Maybe instatiate MeetFinder class?
    #Similar function is in MeetFinder
    #TODO: this function should be in cython
    #Version for reflective border condition
    #May be needed for some implementations (e.g. searching meetings in one box(node) of tree).
    #loop by all different pairs (combinations)
    for (p1, p2) in combinations(particles, 2):
        #TODO: maybe change this line (performance)?
        #Combinations - not needed
        #if p1==p2:
        #    continue

        if meets(p1, p2, rMeet):
            #return (p1, p2)
            return True

    return False

cdef meets(p1, p2, rMeet):
    """ Return True if two particles collide, otherwise return False.
    """
    #return (np.linalg.norm(p2-p1) < self.rMeet)
    #TODO: this function should be in cython
    return (np.linalg.norm(p2-p1) < rMeet)



DTYPE = np.float
DTYPE3 = np.int
ctypedef np.float_t DTYPE_T2
ctypedef np.int DTYPE_T3
#[DTYPE_T2, ndim=2]
#[long, ndim=2]

def checkGridMeetings(np.ndarray particles, np.ndarray gridLocation, float rMeet):
    #TODO: maybe gridLocation - int
    #zipped = np.vstack((gridLocation, particles)) #np.dstack
    #zipped = np.sort(zipped) #FIXME
    #sortind = np.argsort(gridLocation, axis=0)[:,0]
    cdef int i, oldGridRow, count
    cdef np.ndarray oldGrid # = np.zeros([3], dtype=DTYPE3)
    cdef np.ndarray sortind
    cdef np.ndarray particleGridLocation
    cdef bint b
    cdef bint result
    #bint or bool?

    sortind = np.lexsort(np.transpose(gridLocation)[::-1])

    oldGrid = gridLocation[sortind[0]] - [2, 2, 2] #Smaller that all grids (partial order)
    #TODO: oldGrid may be not smaller that all grids


    count = np.shape(particles)[0]

    i = 0
    oldGridRow = 0
    oldGrid = gridLocation[sortind[i]]

    i += 1
    while i<count:
        #row = zipped[i]
        #particle = particles[sortind[i]]
        particleGridLocation = gridLocation[sortind[i]] #row[1]

        ifSameGrid = True
        for j in range(0, 3):
            if oldGrid[j] != particleGridLocation[j]:
                #print "j = {}".format(j)
                ifSameGrid = False
                break

        if not ifSameGrid:
            #Check all meetings for particles from one grid box
            #TODO: check if more than one particle
            #print particles[sortind[oldGridRow:i]]
            result = checkAllMeetings(particles[sortind[oldGridRow:i]], rMeet)
            if result!=False:
                return result

            oldGrid = particleGridLocation
            oldGridRow = i

        i += 1

    result = checkAllMeetings(particles[sortind[oldGridRow:]], rMeet)
    if result!=False:
        return result


    return False