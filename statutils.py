import copy
import os, sys

import time

import datetime

if os.environ.get('USE_MY_UTILS_DEV_VERSION')=="1":
    sys.path.append(os.path.expanduser('~/AGH/Python/utils'))
    #print "test"
    pass
elif os.environ.get('USE_MY_UTILS_DEV_VERSION')=="2":
    print "for this value of USE_MY_UTILS_DEV_VERSION program exit"
    raise Exception("Wrong USE_MY_UTILS_DEV_VERSION")
else:
    sys.path.append(os.path.expanduser('./utils-copy'))
    sys.path.append(os.path.expanduser('./utils'))
    #sys.path.append(os.path.expanduser('./utils-local'))
    pass

from myprogressbar import EmptyProgressBar

from mainutils import *
from geomutils import *



class ResultStatUtils:
    """ Class for some plots, performance measure
            and other statistics.
    """

    #TODO: time of meeting - how depends on
    #   minSpeed, maxSpeed, rMeet, rangeMax and particlesNumber parameters

    @staticmethod
    @listify
    def runResultTest(testCaseCreator, repeat=3):
        """ Run code multiple times, comparing results.
            This function should be use if code is nondeterministic
            :param testCaseCreator: zero-argument function -
                constructor of test case/test code/test function
            :param repeat: how many times repeat running function
            :results: list of results (maybe some transformation of mutable object changed by procedure)
        """
        #Should results be transformed before returning
        #Repeat may be not needed if function itself has option of running it multiple times
        #   It is likely to be true.

        for i in xrange(repeat):
            if i%10 == 0:
                print "Test number: {}\r".format(i)
            #TODO: named parameters
            #Initialize tested function
            #Initializatino may be lambda without arguments
            bm = testCaseCreator()

            #timeit was not working on my notebook - small result, often negative
            #start = timeit.timeit()
            start = time.time()

            result = bm() #bm.run(); run function
            #Maybe run method will be more intuitive?

            #end = timeit.timeit()
            end = time.time()
            #metricResult = bm.meetCount

            testTimeResult = end - start

            #print "Result: {}".format(result)
            result = bm.calcResultMetric(result)
            print "Result metric: {}".format(result)


            #Convert time of simulation to needed format (needed metric)
            #Maybe not needed? - because time is not returned (only resultMetric)
            if bm.steps!=0:
                testTimeResult = testTimeResult / bm.steps
            else:
                #Special case
                testTimeResult = 0

            #Append average step time
            yield result

    @staticmethod
    def measureResultOfCode(testedFun, testedParamValues, seriesParamValues=None, aggregateResults=lambda x: x, repeat=3, progressbar=EmptyProgressBar(), saveToFile=False, filename=None, plot=True, name=None):
        """ Measure results of some code (function; algorithm) in dependency of parameters.
            Plot results (2D). Measure different configuration by series.

        :param testedFun: object(function) to be configured and run for measurement
        :param testedParamValues: list of values of first parameter of function;
            represents parameter which we want to plot
        :param seriesParamValues: list of values of second parameter of function;
            represents function configuration or different algorithm
        :param repeat: used if function is nondeterministic
        """
        #TODO: object should have functions to return default (actual?) function parameters
        #   and tested parameters and series parameters names
        #Function may be used when we want to see how result (transformed)
        # is dependent on parameters
        #def innerLoop()

        if saveToFile==True and filename==None:
            d = datetime.datetime.now()
            filename = "codeResultMeasurement_{}.txt".format(d.strftime("%y-%m-%d-%H%M%S"))

        if saveToFile==True:
            outputFile = open(filename, "w")
            #TODO: not create fo;e when measurement interrupted?
            #TODO: maybe save some name of measurment to file

        seriesCount = len(seriesParamValues)
        paramValuesCount = len(testedParamValues)

        #if series==None:
        pbar = progressbar
        pbar.init(maxval=seriesCount*paramValuesCount)
        #Maybe multilevel progressbar?
        #Maybe methods pbar.add(lastStepValue)
        #TODO: problems with progressbar, when some data is printed
        #TODO: DotProgressbar for returning percent of work done
        #   1%..........
        #   2%..........
        #TODO: estimated computation time
        testedFunPrepared = testedFun(testedParamValues[0], seriesParamValues[0])

        text01 = "Name of plot: {}".format(name)
        print text01
        text1 = "Tested function configuration parameters: {}".format(testedFunPrepared.params)
        print text1
        text2 = "Repeat: {}".format(repeat)
        print text2
        text3 = "Tested param name: {}".format(testedFunPrepared.testedParamName)
        print text3
        print "Tested param values: {} ".format(testedParamValues)
        text5 = "Tested series name: {}".format(testedFunPrepared.seriesParamName)
        print text5

        if saveToFile==True:
            outputFile.write(text01)
            outputFile.write("\n")
            outputFile.write(text1)
            outputFile.write("\n")
            outputFile.write(text2)
            outputFile.write("\n")
            outputFile.write(text3)
            outputFile.write("\n")
            outputFile.write("Tested param values: \n{}\n\n".format(testedParamValues))
            outputFile.write(text5)
            outputFile.write("\n")

        startTime = time.time()

        seriesIndex = 0
        for series in seriesParamValues:
            results = []

            paramValueIndex = 0
            for n in testedParamValues:
                print "Tested param value: {}".format(n)
                #print "testedParticlesNb: {0}".format(n)
                testResults = ResultStatUtils.runResultTest(
                        lambda: testedFun(n, series), repeat=repeat)

                #assert len(testResults)==1 #Now the only possibility
                #print "testRepetition: {0}, result: {1}".format(i, testResult)
                #In this case testResult may represent probability (or average result)

                print (n, testResults[0:10], "...")
                results.append(testResults)

                pbar.update(seriesIndex*seriesCount + paramValueIndex)

                paramValueIndex += 1

            #Average result from repeated results is taken
            #Why average? Maybe min or max? Or median?
            #avgResults = map(np.mean, results)
            aggregatedResults = map(aggregateResults, results)

            #r should be a number
            #for r in results:
            #    assert(len(r)==1)

            if saveToFile==True:
                outputFile.write("Series value: {}\nAggregated results for this series: {}\n".format(series, map(lambda x: "{:.4f}".format(x), aggregatedResults)))

            print aggregatedResults

            if plot==True:
                plt.plot(testedParamValues, aggregatedResults, label=series)

            seriesIndex += 1

        endTime = time.time()
        measurementTime = endTime - startTime
        print "Measurement time: {}".format(measurementTime)
        if saveToFile==True:
            outputFile.write("Measurement time: {}\n".format(measurementTime))

        if saveToFile==True:
            outputFile.close()
        pbar.finish()

        #TODO: plot scale
        if plot==True:
            plt.show()
            raw_input("End of computing statistics (press any key).")



class TimeStatUtils:
    """ Class for some plots, performance measure
            and other statistics.
    """

    #It seemed to work

    #TODO: time of meeting - how depends on
    #   minSpeed, maxSpeed, rMeet, rangeMax and particlesNumber parameters

    @staticmethod
    @listify
    def runPerformanceTest(testCaseCreator, repeat=3):
        for i in xrange(repeat):
            #TODO: named parameters
            #Initialize tested function
            #Initialization may be lambda without arguments
            bm = testCaseCreator() #

            #timeit was not working on my notebook - small result, often negative
            #start = timeit.timeit()
            start = time.time()

            bm() #bm.run(); run function
            #Maybe run method will be more intuitive?

            #end = timeit.timeit()
            end = time.time()

            testTimeResult = end - start

            #Convert time of simulation to needed format (needed metric)
            if bm.steps!=0:
                testTimeResult = testTimeResult / bm.steps
            else:
                #Special case
                testTimeResult = 0

            #Append average step time
            yield testTimeResult

    @staticmethod
    def measureTimeOfCode(testedFun, testedParamValues, seriesParamValues=None, repeat=3):
        """ Measure time of some code (function; algorithm).
            Plot times (2D). Measure different configuration by series.

        :param testedFun: function to be measured
        :param testedParamValues: list of values of first parameter of function;
            represents size of problem
        :param seriesParamValues: list of values of second parameter of function;
            represents function configuration or different algorithm
        :return: nothing, plot metrics before return
        """
        #def innerLoop()

        #if series==None:

        for series in seriesParamValues:
            times = []

            for n in testedParamValues:
                #print "testedParticlesNb: {0}".format(n)
                testTimeResults = TimeStatUtils.runPerformanceTest(
                        lambda: testedFun(n, series), repeat=repeat)

                #print "testRepetition: {0}, result: {1}".format(i, testResult)

                print (n, testTimeResults)
                times.append(testTimeResults)

            #TODO: divide time by simulation steps

            avgTimes = map(np.mean, times)
            print avgTimes

            plt.plot(testedParamValues, avgTimes, label=series)

        plt.show()

        raw_input("abc")

    @staticmethod
    def testedFunctionClosure(testedFun, closureParams, defaultValues):
        """ Create closure of tested function for limiting parameters
            that will be tested in one test.
        :param testedFun: function to be measured (with multiple parameters (e.g. configuration).
        :param closureParams: indices of parameters that we want to test in one test
        :param defaultValues: default values of all parameters (list - len(defaultValues=len(args))
        :return:
        """
        values = copy.copy(defaultValues)

        def closuredFunction(*args):
            #TODO: this function may have not good performance?

            for i in range(len(args)):
                 #Assign arguments from function in good order
                 index = closureParams[i]
                 values[index] = args[i]

            return testedFun(*values)

        return closuredFunction
