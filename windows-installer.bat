::This is windows installer of dependencies for ParticlesCmd and ParticlesGUI programs
::
::Requirements of installer:
::This installer is supposed to be run 
:: from one of Python distributions (Anaconda, WinPython, PythonXY)
::This installation was only partly tested (on Anaconda 2.5.0)
::
::Usage:
::Run ".\windows-installer.bat" from Windows Cmd or Powershell

pip install argparse
pip install progressbar
pip install pycontracts
pip install plyfile