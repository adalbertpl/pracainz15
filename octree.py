#-*- coding: utf-8 -*-
from __future__ import print_function, division

import os
import sys

sys.path.append(os.path.expanduser('~/AGH/Python/utils'))

import itertools
from collections import deque

from contracts import contract

import numpy as np

from mainutils import *
from geomutils import *
from geom3d import *

#from meetfinder import Particle







#TODO: sphinx documentation
class ParticlesOctree:
    """ Class implementing Octree structure for use in Particles program.

    >>> position = (0, 0, 0)
    >>> size = (1, 1, 1)
    >>> octree = ParticlesOctree(position, size)
    >>> particles = map(Particle, np.random.uniform(0.1, 0.9, (1000, 3)))
    >>> particles[0] = Particle(np.array([0.04, 0.5, 0.5]))
    >>> octree.add(particles[0])
    >>> box = octree.findBox(particles[0].position)
    >>> np.allclose(box.position, position)
    True
    >>> np.allclose(box.size, size)
    True
    >>> len(octree.leafs)
    1
    >>> octree.addToBox(box, particles[1])
    >>> len(octree.leafs)
    1
    >>> len(octree.leafs[0].particles)
    2
    >>> octree.traverse(lambda x: print(np.allclose(box.size, size)))
    True
    >>> boxes = octree.findbbox(Range3d(np.array([[0.01, 0.41, 0.41], [0.08, 0.6, 0.6]])))
    >>> len(list(octree._getAll())) >= len(boxes)
    True
    >>> len(boxes) > 0
    True
    >>>
    #>>> len(sum(map(lambda x: print(x.particles), boxes)))
    #1
    >>> octree.removeFromBox(box, particles[0])
    >>> octree.remove(particles[1])
    >>> len(octree.leafs)
    1
    >>> for particle in particles:
    ...     octree.add(particle)
    >>> len(octree.leafs) > 3
    True
    >>> len(list(octree._getAll())) > len(octree.leafs)
    True
    >>> boxes = octree.findbbox(Range3d(np.array([[0.1, 0.1, 0.1], [0.44, 0.44, 0.44]])))
    >>> len(octree.leafs)*3/4 > len(boxes) > 3
    True
    >>> octree.root.countSum
    1000
    """

    def __init__(self, position, size, minCount=4, maxCount=8):
        """ Initialize octree structure.
        :param position: "Smallest" point of root box
        :param size: list of lengths of box edges
        :return:
        """
        #MinCount - maybe 16? MaxCount - maybe 32?
        #Why this two values are needed here, not only in root?
        self.position = np.array(position)
        self.size = np.array(size)
        self.root = Node(None, self.position, self.size)

        self.minCount = minCount
        self.maxCount = maxCount

    def __str__(self):
        return "ParticlesOctree(root={})".format(self.root)

    def add(self, particle):
        box = self.findBox(particle.position)
        self.addToBox(box, particle)

    def remove(self, particle):
        box = self.findBox(particle.position)
        self.removeFromBox(box, particle)

    def findBox(self, position):
        """ Find leaf containing position.
        :param position: x, y, z as ?
        :return: leaf Node containing position
        """
        box = self.root
        while(box.hasChildren()):
            box = self._getChild(box, position)

        return box

    #TODO: refactor Range to class or to class of static methods


    def knn(self, k, particle):
        """ Find k nearest neighbours from one particle."""
        raise Exception("Not implemented")
        pass

    def findbbox(self, range3d):
        """ Return list of leaf boxes which can have points in range3d bounding box."""

        results = []
        queue = deque()

        queue.append(self.root)
        while (queue):
            box = queue.pop()

            if box.hasChildren():
                for child in box.children:
                    #TODO: change?; box contained in range
                    if range3d.intersects(child.range):
                        queue.append(child)

            else:
                results.append(box)

        return results

    def _getChild(self, box, position):
        relativePos = position - box.position
        #gridIndex = np.floor_divide(relativePos, (box.size/2))
        gridIndex = relativePos // (box.size/2)
        return box.childrenDict[tuple(gridIndex)] #check

    def traverse(self, action=lambda x:None):
        """
        Traverse all leafs and do some action.
        :return:
        """
        for box in self.leafs:
            action(box)

    def _getAll(self):
        """ Get all nodes DFS (with nonleaf too). """
        stack = deque()
        #TODO: check
        stack.append(self.root)

        while stack:
            box = stack.pop()
            for child in box.children:
                stack.append(child)
            yield box

    def addToBox(self, box, particle): #position
        box.particles.add(particle)

        #from collections import deque
        #q = queue(box)
        #while q:

        if (self._ifShouldDivide(box)):
            self._divideBox(box)



    #Kontrakt - after each public method - nonleaf node has zero particles
    def _divideBox(self, box):
        size = box.size / 2
        grid = Grid(box.position, size)

        for element in itertools.product([0, 1], [0, 1], [0, 1]):
            child = Node(box, grid.get(element), size)
            #box.children.append(child)
            box.childrenDict[element] = child

        for particle in box.particles:
            newBox = self.findBox(particle.position)
            newBox.particles.add(particle)
            #Maybe sort particles colletion by x?

        box.particles.clear()

        for child in box.children:
            if (self._ifShouldDivide(child)):
                self._divideBox(child)


    def _ifShouldDivide(self, box):
        return (box.countSum > self.maxCount)


    #def getNb(self, position, direction):
        #not easy in implementing?

    def removeFromBox(self, box, particle):
        #Remove from this box, not from child
        box.particles.remove(particle)

        checkedBox = box
        while ((checkedBox!=None) and self._ifShouldCompact(checkedBox)):
            if box.hasChildren():
                self._compactBox(checkedBox)

            #if checkedBox.hasParent():
            checkedBox = checkedBox.parent #maybe self.parent(checkedBox)?

    def _ifShouldCompact(self, box):
        if box.countSum < self.minCount:
            return True
        else:
            return False

    def _compactBox(self, box):
        #Box should not have more than one level of children?
        for child in box.children:
            self._compactBox(child)

        for child in box.children:
            #TODO: maybe change for better performance?
            box.particles.extend(child.particles)

        box.childrenDict.clear() #TODO: check


    #Podczas kompaktowania trzeba też sprawdzić wyższe poziomy, ale sprawdzenie każdego poziomu nie zajmuje zbyt dużo czasu.

    #Trzeba unikać nadmiarowego kopiowania cząstek przy tworzeniu
    #i łączeniu boksów.
    #Jeśli zbiór byłby niemutowalny, to byłoby lepiej.
    #Jeśli jest mutowalny, to trzeba często

    # def _setBox(self, particle, box):
    #     pass

    #def lookForBox(self, posit):

    def getBox(self, particle):
        #maybe position, not particle
        return self.findBox(particle.position)

    def moveParticle(self, particle, newPosition):
        """ Move particle to newPosition and optionally change box."""
        oldBox = self.getBox(particle)
        newBox = self.findBox(newPosition)
        if oldBox!=newBox:
            self.changeBox(particle, newPosition)
        else:
            particle.position = newPosition

    def changeBox(self, particle, newPosition):
        """ Change box from old to new. New box may be same as old.
            Position of particle is changed to newPosition"""
        #highlevel method
        oldBox = self.getBox(particle)
        self.removeFromBox(oldBox, particle)

        foundBox = self.findBox(newPosition)
        particle.position = newPosition
        self.addToBox(foundBox, particle)
        #self.setBox - nie pasuje tutaj; jedynie symetria wskazuje, ze powininen byc

    @property
    def leafs(self):
        leafs = []
        for box in self._getAll():
            if not box.hasChildren():
                leafs.append(box)

        return leafs

#FIXME: ParticlesOctree not finding collision yet. But moving particles works good.

class Node:
    """ Node represents one octree box.
        Node may be leaf or nonleaf.
    """

    def __init__(self, parent, position, size):
        self.parent = parent
        #self.count = 0
        #are setters and getters needed fod children and particles?
        self.childrenDict = {}
        self.particles = set()

        #Position and size of node should be unchanged
        self.position = position
        self.size = size
        self.range = Range3d(np.array([self.position, self.position+self.size]))

    def __str__(self):
        return "Box(id={}, particles={}, children={})".format(id(self), self.particles, map(id, self.children))

    #def divide(self):
    #def compact(self):

    @property
    def children(self):
        #TODO: set, del
        return self.childrenDict.values()

    # def isLeaf(self):
    #     return (not self.hasChildren())

    @property
    def level(self):
        if not self.hasChildren():
            return 0
        else:
            m = max(map(lambda x:x.level, self.children))
            return min([m+1, 2])

    @property
    def countSum(self):
        result = len(self.particles)
        result += sum(map(lambda x:x.countSum, self.children))
        return result

    @property
    def allParticles(self):
        #TODO: maybe iterator
        if self.hasChildren:
            for child in self.children:
                for particle in child.allParticles:
                    yield particle
        else:
            for particle in self.particles:
                yield particle

    @property
    def range(self):
        return self.range

    @property
    def start(self):
        return self.position

    @property
    def end(self):
        return self.position + self.size

    #def inside(self, position):
    #def add(self):

    def hasParent(self):
        return (self.parent != None)

    def hasChildren(self):
        return len(self.children) > 0